This code is documented with Doxygen. Navigate to ./docs/html/index.html to open the site.

To quickly compile, run:

    make
    
for a debug version of the code. 

For high performance, run:
    make release
    
which is much faster and optimized, but without debuf info.
