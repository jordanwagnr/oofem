var searchData=
[
  ['phi',['phi',['../classfem_1_1_w_m_fractal.html#aad5ae7c93f20be7c4db4b8030db3aacc',1,'fem::WMFractal::phi()'],['../classfem_1_1_finite_element_space.html#a98230a1fc2dad4cc5a86cd7bf1544401',1,'fem::FiniteElementSpace::phi()']]],
  ['pi',['pi',['../common_8h.html#a1daf785e3f68d293c7caa1c756d5cb74',1,'common.h']]],
  ['point',['Point',['../classfem_1_1_point.html',1,'fem']]],
  ['point',['point',['../classfem_1_1_mesh.html#af412e6d737d870650f15b9636d5a6565',1,'fem::Mesh::point()'],['../classfem_1_1_mesh_entity.html#aac8f9cef46fec73fbb9c1f85e0ac7082',1,'fem::MeshEntity::point()'],['../classfem_1_1_point.html#a9276b2c116a2ead8ee85defd19177003',1,'fem::Point::Point()'],['../classfem_1_1_point.html#a3cca0db3fb5d860c7c3b78ec5103a842',1,'fem::Point::Point(Eigen::Ref&lt; Eigen::VectorXd &gt;)'],['../classfem_1_1_point.html#a1399b5a7c466c3e5795a5471646e83a4',1,'fem::Point::Point(double x)'],['../classfem_1_1_point.html#a74011876787f3117039deff496b23501',1,'fem::Point::Point(double x1, double x2)'],['../classfem_1_1_point.html#a39d2ef969024dc6077d8aa843ee0f88e',1,'fem::Point::Point(double x1, double x2, double x3)'],['../classfem_1_1_point.html#ad34ee58bacb057b19d950817fd77b2ed',1,'fem::Point::Point(const Point &amp;p)'],['../namespacefem.html#add10676a42b227eaa41b6ed42d49cc14a3409507532d51f6d8f9cf31df189e45e',1,'fem::POINT()']]],
  ['point_2ecpp',['Point.cpp',['../_point_8cpp.html',1,'']]],
  ['point_2eh',['Point.h',['../_point_8h.html',1,'']]],
  ['points',['points',['../classfem_1_1_quadrature.html#a73709b3a004ee8f23ebb3aa71e33fd1c',1,'fem::Quadrature']]],
  ['position',['position',['../classfem_1_1_mesh_entity_iterator.html#a25ec98fbb94af80e92d36e9a380fc4e0',1,'fem::MeshEntityIterator']]],
  ['print',['print',['../classfem_1_1_mesh.html#af5a35e846fe102b2b2c0232baf83f6cf',1,'fem::Mesh::print()'],['../classfem_1_1_mesh_tag.html#a93ffa4e41b50ffa5009c282118baa29b',1,'fem::MeshTag::print()']]],
  ['printtopology',['printTopology',['../classfem_1_1_mesh.html#aa1db89c118e236f6f58aef1cba433ef7',1,'fem::Mesh']]],
  ['problem_20gallery',['Problem Gallery',['../probs.html',1,'']]],
  ['project',['project',['../classfem_1_1_finite_element_space.html#a659ab1a1ced1094c9ce4db7a14040748',1,'fem::FiniteElementSpace']]],
  ['pushforward',['pushForward',['../classfem_1_1_isoparametric_mapping.html#a599f6cf536ce00f61896f1aa8193a877',1,'fem::IsoparametricMapping::pushForward(ScalarFunction &amp;f, Point x, MeshEntity &amp;e)'],['../classfem_1_1_isoparametric_mapping.html#a9fa9a339fb2d401fcfaacf51501fdcc7',1,'fem::IsoparametricMapping::pushForward(ScalarFunction &amp;f, Quadrature &amp;q, MeshEntity &amp;e)']]]
];
