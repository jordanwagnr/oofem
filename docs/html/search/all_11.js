var searchData=
[
  ['qpoints',['qpoints',['../classfem_1_1_quadrature.html#a8d6f3aa406615d20b740f7c3fe7fac4c',1,'fem::Quadrature']]],
  ['qpts',['qpts',['../classfem_1_1_linear_form_integrator.html#a088ae4a6e9c95cbef0070dc9ecfd6bba',1,'fem::LinearFormIntegrator::qpts()'],['../classfem_1_1_bilinear_form_integrator.html#a68971294ebae2fe642e3e808ea24b00f',1,'fem::BilinearFormIntegrator::qpts()']]],
  ['quad',['quad',['../classfem_1_1_linear_form_integrator.html#aff150c116276651f6eaf7e544e225bee',1,'fem::LinearFormIntegrator::quad()'],['../classfem_1_1_bilinear_form_integrator.html#aab0f2eb8978ee898a914ab53d200efe4',1,'fem::BilinearFormIntegrator::quad()'],['../namespacefem.html#add10676a42b227eaa41b6ed42d49cc14a4be30e5cba1d847355159247713b6c89',1,'fem::QUAD()']]],
  ['quadrature',['Quadrature',['../classfem_1_1_quadrature.html',1,'fem']]],
  ['quadrature',['Quadrature',['../classfem_1_1_quadrature.html#a2fa8842eb9bdb90fa46a887996d14687',1,'fem::Quadrature::Quadrature()'],['../classfem_1_1_quadrature.html#a5a10a0cbfab88b8c014b964069b11187',1,'fem::Quadrature::Quadrature(std::vector&lt; Point &gt; &amp;q, std::vector&lt; double &gt; &amp;w, GEOM_t g)']]],
  ['quadrature_2ecpp',['Quadrature.cpp',['../_quadrature_8cpp.html',1,'']]],
  ['quadrature_2eh',['Quadrature.h',['../_quadrature_8h.html',1,'']]]
];
