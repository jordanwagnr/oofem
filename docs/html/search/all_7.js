var searchData=
[
  ['g',['G',['../classfem_1_1_w_m_fractal.html#a581cb81dfc26a89dbe2635e41c4dd2fc',1,'fem::WMFractal::G()'],['../classfem_1_1_dirichlet_b_c.html#a44c0fb03f3a5ffeecf0bfeafbde150ba',1,'fem::DirichletBC::g()']]],
  ['gallerypage_2eh',['gallerypage.h',['../gallerypage_8h.html',1,'']]],
  ['gamma',['gamma',['../classfem_1_1_w_m_fractal.html#ae8318ac0b00b644d3ff73733d7e7448f',1,'fem::WMFractal']]],
  ['gaussquadrature',['GaussQuadrature',['../classfem_1_1_gauss_quadrature.html',1,'fem']]],
  ['gaussquadrature',['GaussQuadrature',['../classfem_1_1_gauss_quadrature.html#a6a0987b5c2a305ae3a5c96a92b395d9e',1,'fem::GaussQuadrature']]],
  ['geom_5ft',['GEOM_t',['../namespacefem.html#add10676a42b227eaa41b6ed42d49cc14',1,'fem']]],
  ['gettag',['getTag',['../classfem_1_1_mesh.html#a11c5cabef53dfbf237fcd7cdacf2ac1d',1,'fem::Mesh']]],
  ['gettaggeddofs',['getTaggedDofs',['../classfem_1_1_finite_element_space.html#a2645fa6b0b412d2f31151b44685c8613',1,'fem::FiniteElementSpace']]],
  ['globalnodelayout',['globalNodeLayout',['../classfem_1_1_finite_element_space.html#a7fe4fc923d63b21aa2b5ec28e21e5711',1,'fem::FiniteElementSpace']]],
  ['gmshreader',['GmshReader',['../classfem_1_1_mesh.html#a8445c3fe3cfa5844f03944dceabe0d06',1,'fem::Mesh::GmshReader()'],['../classfem_1_1_gmsh_reader.html#ad159446c4431b9eae1b682fef6acda17',1,'fem::GmshReader::GmshReader()'],['../classfem_1_1_gmsh_reader.html#a2f31906f0894b7b2d728c4b79439203b',1,'fem::GmshReader::GmshReader(Mesh *m, std::string)']]],
  ['gmshreader',['GmshReader',['../classfem_1_1_gmsh_reader.html',1,'fem']]],
  ['gnodelayout',['gnodelayout',['../classfem_1_1_finite_element_space.html#a5dfd41ce6eacec439d0f45ae3e3ce0c7',1,'fem::FiniteElementSpace']]],
  ['gotosection',['goToSection',['../classfem_1_1_gmsh_reader.html#a51646fcabef369272d0fb48e36846df8',1,'fem::GmshReader']]]
];
