var searchData=
[
  ['offset',['offset',['../classfem_1_1_dof_map.html#ab411d36dead1f4c9ee4630443d9256ba',1,'fem::DofMap::offset(Cell &amp;c)'],['../classfem_1_1_dof_map.html#a30e311aef006f630c16209e21f19b9fc',1,'fem::DofMap::offset(Facet &amp;f)'],['../classfem_1_1_dof_map.html#aebbdd2130733c923cb909a685814b46b',1,'fem::DofMap::offset(Vertex &amp;v)']]],
  ['offsets',['offsets',['../classfem_1_1_mesh.html#aded81c91366b951575e2a31c41207143',1,'fem::Mesh']]],
  ['operator_21_3d',['operator!=',['../classfem_1_1_mesh_entity_iterator.html#aed53ab78e1d9136dde4a8401170a8b3c',1,'fem::MeshEntityIterator']]],
  ['operator_28_29',['operator()',['../classfem_1_1_expression.html#a47437e2682b192f1d18aa44e822e59a3',1,'fem::Expression::operator()()'],['../classfem_1_1_finite_element_function.html#a17a014d841c2e29ca4bb1f4e15620094',1,'fem::FiniteElementFunction::operator()()'],['../classfem_1_1_isoparametric_mapping.html#aee6c12520c04112ba07cf07c9832a209',1,'fem::IsoparametricMapping::operator()()'],['../classfem_1_1_point.html#af5c3c5cb03616d54224a729dcffa4b99',1,'fem::Point::operator()()'],['../classfem_1_1_point.html#ac95a94a9a0a230b3f05d5751d5c8c235',1,'fem::Point::operator()(int c)'],['../classfem_1_1_point.html#a0292fe128b091e8a6904e7a5fba25eea',1,'fem::Point::operator()(Eigen::Ref&lt; Eigen::VectorXd &gt; x)']]],
  ['operator_2a',['operator*',['../classfem_1_1_mesh_entity_iterator.html#af67a9ac082ea7cb003fea5675c207308',1,'fem::MeshEntityIterator::operator*()'],['../classfem_1_1_point.html#a973c99c47bfbe880df65c8fa4c0fe70c',1,'fem::Point::operator*()']]],
  ['operator_2b',['operator+',['../classfem_1_1_point.html#a7e301727fa7b3d9eac40fc43bb785af3',1,'fem::Point']]],
  ['operator_2b_2b',['operator++',['../classfem_1_1_mesh_entity_iterator.html#a831c337a44cdd3e4f4981e90d76ccdbc',1,'fem::MeshEntityIterator']]],
  ['operator_2d_2d',['operator--',['../classfem_1_1_mesh_entity_iterator.html#a0056aa21021e6484144e1d3f7f6259ff',1,'fem::MeshEntityIterator']]],
  ['operator_2d_3e',['operator-&gt;',['../classfem_1_1_mesh_entity_iterator.html#a673173edc46e0908da39d5c797fbe15d',1,'fem::MeshEntityIterator']]],
  ['operator_3d',['operator=',['../classfem_1_1_point.html#ae2cdc19aed90303cdcee09def0b153f0',1,'fem::Point']]],
  ['operator_3d_3d',['operator==',['../classfem_1_1_mesh_entity_iterator.html#aa2f4a63cf9146feb3b32430f8a37b78e',1,'fem::MeshEntityIterator']]],
  ['operator_5b_5d',['operator[]',['../classfem_1_1_finite_element_function.html#a693cc34b474ca81576deeb911927a489',1,'fem::FiniteElementFunction']]]
];
