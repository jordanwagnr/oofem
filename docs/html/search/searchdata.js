var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvw",
  1: "bcdefgilmpqvw",
  2: "f",
  3: "cdefgimpqu",
  4: "abcdefghijlmnopqrstuvw",
  5: "_acdfgijklmnopqrtuvw",
  6: "cfsv",
  7: "efg",
  8: "cflnpqtv",
  9: "bcdfgilmov",
  10: "p",
  11: "cdptu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Pages"
};

