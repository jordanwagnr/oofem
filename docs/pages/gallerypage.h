/**
 * @page probs Problem Gallery
 * 
 *  @section toc Contents
 *  - @ref sc
 *  - @ref disk
 *  - @ref co
 * 
 * As of right now, the only integrators implemented are constant-coefficient and 
 * variable-coefficient diffusion operators, as well as body force functionals. These can easily
 * be extend to other operations by deriving from \ref fem.BilinearFormIntegrator and 
 * \ref fem.LinearFormIntegrator, respectively.
 * 
 * While we can only solve two types of problems at the moment, we have tremendous flexibility in
 * the domains we solve over, coefficient and functions, and where to apply boundary conditions. 
 * 
 * @page sc Unit square with three Dirichlet boundaries and 1 Neumann boundary
 \code

        #include "Mesh.h"
        #include "Point.h"
        #include "Form.h"
        #include "FiniteElementFunction.h"
        #include "DirichletBC.h"
        #include "Quadrature.h"
        #include "IsoparametricMapping.h"
        #include "DataWriter.h"
        #include <Eigen/SparseCholesky>

        using namespace std;
        using namespace fem;

        int main(int argc, char *argv[]){
            
            // Construct the mesh and print the information.
            Mesh mesh("meshes/square_fine.msh");
            mesh.print();

            // Construct the finite element space over the mesh
            FiniteElementSpace fes(mesh);

            // Constrain dofs that are related to specific parts of the mesh that have been tagged by GMSH
            set<int> cdof = fes.getTaggedDofs(2, 1);
            fes.constrainDofs(cdof);
            fes.buildIdArray();

            // Construct a quadrature rule
            GaussQuadrature q(TRIA, 3);
            
            // construct a constant forcing and coefficient function
            Constant f(.5);
            Constant k(.1);
            Constant z(0); // zero function

            // Project the zero function onto the constructed finite element space, so that we have
            // zeros defined on the constrained nodes
            FiniteElementFunction u = fes.project(z);
            
            // Create the bilinear form and add a diffusion integrator
            BilinearForm A(fes);
            A.addDomainIntegrator(new DiffusionIntegrator(k,q));
            A.assemble();

            // Create the linear form and add a load integrator
            LinearForm L(fes);
            L.addDomainIntegrator(new LoadIntegrator(f,q));
            L.assemble();

            // Create a linear solver context via Eigen. 
            Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>> solver;
            // Factor the matrix
            solver.compute(A.matrix());
            // Solve the system for the function coefficients
            Eigen::VectorXd &x = u.vcoeffs();
            x = solver.solve(L.coeffs());

            // Export the solution for visualization
            DataWriter writer("run", u);
            writer.writeAllData();
        }
 * \endcode
 * 
 * \image html figure1.png "Triangular mesh" 
 * 
 * \image html figure2.png "Quadrilateral mesh"
 * 
 * 
 * 
 * @page disk Circular Disk with fractal coefficient function
 * 
\code
 #include "Mesh.h"
 #include "Point.h"
 #include "Form.h"
 #include "FiniteElementFunction.h"
 #include "DirichletBC.h"
 #include "Quadrature.h"
 #include "IsoparametricMapping.h"
 #include "DataWriter.h"
 #include <Eigen/SparseCholesky>

 using namespace std;
 using namespace fem;

 int main(int argc, char *argv[]){
    
 // Construct the mesh and print the information.
 Mesh mesh("meshes/disc.msh");
 mesh.print();

 // Construct the finite element space over the mesh
 FiniteElementSpace fes(mesh);

 // Constrain dofs that are related to specific parts of the mesh that have been tagged by GMSH
 set<int> cdof = fes.getTaggedDofs(1, 1);
 fes.constrainDofs(cdof);
 fes.buildIdArray();

 // Construct a quadrature rule
 GaussQuadrature q(TRIA, 3);
    
 // construct a constant forcing and coefficient function
 Constant f(.5);
 WMFractal k(.03, 2.2);
 Constant z(0); // zero function

 // Project the zero function onto the constructed finite element space, so that we have
 // zeros defined on the constrained nodes
 FiniteElementFunction u = fes.project(z);

 // Create the bilinear form and add a diffusion integrator
 BilinearForm A(fes);
 A.addDomainIntegrator(new DiffusionIntegrator(k,q));
 A.assemble();

 // Create the linear form and add a load integrator
 LinearForm L(fes);
 L.addDomainIntegrator(new LoadIntegrator(f,q));
 L.assemble();

 // Create a linear solver context via Eigen. 
 Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>> solver;
 // Factor the matrix
 solver.compute(A.matrix());
 // Solve the system for the function coefficients
 Eigen::VectorXd &x = u.vcoeffs();
 x = solver.solve(L.coeffs());

 // Export the solution for visualization
 DataWriter writer("run", u);
 writer.writeAllData();

 // Export coefficient for visualization
 FiniteElementFunction kappa = fes.project(k);
 DataWriter coeffwriter("kappa", kappa);
 coeffwriter.writeAllData();

 }
\endcode

* \image html figure3.png "Disc with fractal-valued coefficient function"
* \image html figure4.png "Fractal coefficient function projected onto the space"


/**
 * @page co Disk with square cutout and mesh refinement
 * 

\code
#include "Mesh.h"
#include "Point.h"
#include "Form.h"
#include "FiniteElementFunction.h"
#include "DirichletBC.h"
#include "Quadrature.h"
#include "IsoparametricMapping.h"
#include "DataWriter.h"
#include <Eigen/SparseCholesky>

using namespace std;
using namespace fem;

int main(int argc, char *argv[]){
    
    // Construct the mesh and print the information.
    Mesh mesh("meshes/cutout.msh");
    mesh.print();

    // Construct the finite element space over the mesh
    FiniteElementSpace fes(mesh);

    // Constrain dofs that are related to specific parts of the mesh that have been tagged by GMSH
    set<int> cdof1 = fes.getTaggedDofs(1, 1);
    set<int> cdof2 = fes.getTaggedDofs(2,1);
    fes.constrainDofs(cdof1);
    fes.constrainDofs(cdof2);
    fes.buildIdArray();

    // Construct a quadrature rule
    GaussQuadrature q(TRIA, 3);
    
    // construct a constant forcing and coefficient function
    Constant f(10);
    Constant k(1);
    Constant z(0); // zero function

    // Project the zero function onto the constructed finite element space, so that we have
    // zeros defined on the constrained nodes
    FiniteElementFunction u = fes.project(z);

    // Create the bilinear form and add a diffusion integrator
    BilinearForm A(fes);
    A.addDomainIntegrator(new DiffusionIntegrator(k,q));
    A.assemble();

    // Create the linear form and add a load integrator
    LinearForm L(fes);
    L.addDomainIntegrator(new LoadIntegrator(f,q));
    L.assemble();

    // Create a linear solver context via Eigen. 
    Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>> solver;
    // Factor the matrix
    solver.compute(A.matrix());
    // Solve the system for the function coefficients
    Eigen::VectorXd &x = u.vcoeffs();
    x = solver.solve(L.coeffs());

    // Export the solution for visualization
    DataWriter writer("run", u);
    writer.writeAllData();
}
\endcode

\image html figure5.png "Mesh with overly refined mesh resolution"
\image html figure6.png "Solution with homogeneous Dirichlet values on exterior and interior boundaries"
 * 
 **/