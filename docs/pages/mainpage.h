/**
 * @mainpage The Finite Element Method
 *
 * @section toc Contents
 *  - @ref bg
 *  - @ref theory
 *  - @ref usage
 *  - @ref probs
 * 
 * @section bg Background
 *
 * The finite element method is an extremely powerful and general framework for solving partial
 * differential equations or variational problems. In fact, the method is so general that the other
 * common discretization schemes, such as finite difference and finite volume, are in fact
 * themselves special cases of the finite element method. In the following, we will demonstrate the
 * theory of finite elements, which will then lead us to using the code.
 *
 * @subsection theory Theory
 *  
 * Consider the variable coefficient Poission equation: 
 * \f[
 * \nabla \cdot \big( \kappa \nabla u \big)= f(\boldsymbol{x}). 
 * \f]
 * Let us first recall the basics of the finite difference method, where the main point was that
 * we tried to estimate the differential operators themselves with Taylor series expansions. It was
 * not hard to construct an operator, \f$ \bf L: \mathbb{R}^n \rightarrow \mathbb{R}^n \f$, such
 * that its inverse applied to the right hand side resulted in the approximate solution. Also recall
 * that to build this operator we used a stencil that required a structured, uniform grid. We will
 * see that this approach of jumping straight to estimating derivatives is a very restrictive one.
 * 
 * The finite element begins at a much more fundamental level: <b>the weak form</b>. The weak form
 * of an equation results from multiplying the original equation by an arbitrary "test" function and 
 * then integrating the whole thing over the domain. Also, by applying Green's theorem (i.e. 
 * integration by parts), we can write the weak form of the variable-coefficient Poisson equation
 * as
 * \f[ \int_\Omega \kappa \nabla v \cdot \nabla u \ \ d\Omega = \int_\Omega fv \ d\Omega + 
 *      \int_\Gamma \mathbf{n} \cdot (\kappa v \nabla u) \ d\Gamma
 * \f]
 * 
 * The weak form
 * often confuses people, as it seems to be applied for apparently no reason; however, with a bit
 * of functional analysis, the reason for doing so is more clear. What we have actually done here
 * is turn the PDE into an equality of a bilinear form \f$ a: \mathcal{U}\times\mathcal{V} 
 * \rightarrow \mathbb{R} \f$ and a linear form\f$ l: \mathcal{V} \rightarrow \mathbb{R}\f$ such that
 * \f[
 *      a(u,v) = l(v),
 * \f] 
 * where
 * \f[
 *      a(u,v) = \int_\Omega \kappa \nabla v \cdot \nabla u \ d\Omega - 
 *      \int_\Gamma \mathbf{n} \cdot (\kappa v \nabla u) \ d\Gamma
 * \f]
 * \f[
 *      l(v) = \int_\Omega fv \ d\Omega.
 * \f]
 * 
 * Using the fact that the test function is arbitrary, it can be rather simply shown that if a
 * solution \f$ u \f$ satisfies the weak form, then it will also satisfy the original equation. 
 * With more work, it can also be proven to be unique under specific conditions of the Lax-Milgram
 * therom.
 * 
 * At this point, we have not made anything discrete---this all has taken place in infinite
 * dimensional function spaces. To solve this problem with a computer, we need to now make it 
 * discrete. We do this by representing the infinite dimensional solution and test function in a 
 * finite dimensional subspace with a reduced basis,
 * \f[
 *      u^h = \sum U_b \phi_b \quad and \quad  v^h = \sum V_a \psi_a.
 * \f]
 * Due to linearity upon substituting into our problem, we the discrete problem then becomes
 * \f[
 *      \sum_a \sum_b V_a a(\psi_a, \phi_b) U_b = \sum_a V_a l(\psi_a) \\
 *      = \sum_b a(\psi_a, \phi_b) U_b = l(\psi_a).       
 * \f]
 * This is clearly just a matrix system
 * \f[
 *      \mathbf{KU} = \mathbf{F},
 * \f]
 * where \f$ K_{ab} =  a(\psi_a, \phi_a) \f$ and \f$ F_a = l(\psi_a). \f$
 * 
 * There is a huge amount of flexibility in the choices of basis functions; however, the most
 * popular are the so-called hat functions. To assemble the system, one needs to perform an
 * integration for each entry. Again, huge flexibility is available in how this is done; however,
 * Gaussian quadratures are the most popular.
 * 
 * This approach has left us with a very powerful framework. One of the most notable advantages is
 * that it is quite natural to use unstructured meshes, which allow us to solve problems are very
 * complex domains. Also, for self-adjoint problems such as this, the finite element solution can
 * be shown to be optimal solution.
 * 
 * To learn how to use the code, see @ref usage
 * 
 * 
 * 
 * 
 **/