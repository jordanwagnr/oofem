/**
 * @page usage Using the code
 *
 *  @section toc Contents
 *  - @ref ac
 *  - @ref git
 *  - @ref build
 *  - @ref comps
 *  - @ref results
 * 
 * 
 * @subsection ac About the code
 *   
 * The code has been developed in an object oriented manner, where each piece of the finite
 * element process is encoded into objects using the object-oriented capabilities of c++. This
 * allows for this code to be used as a library for building your own solver; each object is
 * merely a building block of the entire process. Furthermore, these classes may be extended by 
 * any user by deriving classes from those already created. The fundamental, low-level building
 * blocks of the finite element method have been generated here, so these should be able to be 
 * used without modification for more complex problems, such as, for example vector-valued
 * problems like the Navier-Stokes equations.
 * 
 * @subsection git Obtaining a copy
 * The code is self-contained (except for plotting, which requires matplotlib) and hosted on
 * bitbucket. The directory can be cloned by copying the following into a terminal window:
 * \verbatim git clone https://jordanwagnr@bitbucket.org/jordanwagnr/oofem.git \endverbatim
 * 
 * @subsection build Building the code
 * The build system is implemented with make. At the moment, only one executable is created, which
 * is generated from main.cpp in source. To create the executable with debug information, a
 * standard call to
 * \verbatim make \endverbatim
 * will create the executable. For high performance, building with 
 * \verbatim make release \endverbatim will build the code without debugging info and with compiler
 * optimizations turned on. Execution of a release build is multiple times faster than the debug
 * build. Once the code compiles an executable named solve will be created in the main directory,
 * which you can run from a terminal with
 * \verbatim ./solve \endverbatim
 * 
 * @subsection comps Major components of a solver
 * Every finite element solver needs several primary components:
 *      -# a mesh to handle geometry and topology, \ref fem.Mesh,
 *      -# a function space over that mesh, \ref fem.FiniteElementSpace,
 *      -# a bilinear form that can be represented by a matrix, \ref fem.BilinearForm,
 *      -# a linear form that can be represented by a vector, \ref fem.LinearForm,
 *      -# and a linear solver to perform spare solves.
 * 
 * While there are several other class that can be used to do different things, these are always 
 * needed in a solve. Information on their use can be found in the code documentation, as well as
 * in the examples.
 * 
 * As of right now, the only mesh types I offer is GMSH, which is a popular, open-source mesh 
 * generator that is extremely powerful once it is learned. During mesh generation, GMSH allows us
 * to manually tag manifolds and submanifolds of the mesh that may correspond to mesh entities that
 * we would like to specify boundary conditions over.
 * 
 * @subsection results Viewing the results
 * The code comes with a class names \ref fem.DataWriter, which will write function data to its
 * own directory. The data files contain the mesh topology and geometry, as well as the field
 * values. If a version of matplotlib is installed, the results can be visualized with a call to
 * \verbatim ./visualize <directory name>.  \endverbatim
 * This small python script is able to visualize scalar fields over two-dimension triangulations or
 * quadralateral meshes.
 * 
 * To see serveral examples, please see the \ref probs
 * 
 **/