CXX = g++
LD = g++
LINK_FLAGS = -std=c++11
LIBS = -lm
INCLUDE_DIRS = \
-I./src/ \
-I/usr/local/lib 

SOURCES = \
$(wildcard src/*.cpp) \
$(wildcard src/*/*.cpp)

OBJ_DIR = ./obj
STRIPPED_OBJECTS = $(notdir $(SOURCES:.cpp=.o))
OBJECTS  = $(addprefix $(OBJ_DIR)/,$(STRIPPED_OBJECTS))

DEPS = $(OBJECTS:.o=.d)
VPATH = src

DBUG = debug

build: $(OBJECTS) ./solve
-include $(DEPS)

# link the object files
./solve: $(OBJECTS)		
	@echo Linking objects...
	$(LD) $(LINK_FLAGS) $(INCLUDE_DIRS) $(OBJECTS) -o $@ $(LIBS)

# build object files
$(OBJECTS): $(OBJ_DIR)/%.o: %.cpp
	@echo Compiling sources...
	mkdir -p $(OBJ_DIR)
	cd src; make $(DBUG)


release: DBUG =
release: $(OBJECTS) ./solve

# clean any files created in a prior compilation
clean:
	rm -rf $(OBJ_DIR)/*.o $(OBJ_DIR)/*.d $(OBJ_DIR)/*.dd ./solve

# compile and link everything
all: clean ./solve

# print macros in make file by calling e.g. make print-OBJECTS
print-%:
	@echo $* = $($*)

docs:
	cd docs; make
