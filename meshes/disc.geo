Point(1) = {-1, 0, -0, 1.0};
//+
Point(2) = {0, -1, -0, 1.0};
//+
Point(3) = {1, 0, -0, 1.0};
//+
Point(4) = {0, 1, -0, 1.0};
//+
BSpline(1) = {1, 2, 3, 4, 1};
//+
Curve Loop(1) = {1};
//+
Plane Surface(1) = {1};
//+
Physical Curve(1) = {1};
//+
Physical Surface(2) = {1};
