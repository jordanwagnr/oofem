#include "DataWriter.h"
#include <cstdlib>

using namespace fem;
using namespace std;
using namespace Eigen;



/**
 * Creates a solution writer
 * 
 * @param u FiniteElementFunction to write
 * 
 **/
DataWriter::DataWriter(string dir, FiniteElementFunction &u){
    uh = &u;
    string f = "mkdir -p " + dir; 
    system(f.c_str());
    topostream.open("./" + dir + "/topo");
    coordstream.open("./" + dir + "/coord");
    fieldstream.open("./" + dir + "/field");
}



/**
 * Writes the element topology as basic cell-vertex covering relations
 * 
 * 
 **/
void DataWriter::writeElementTopology(){
    Mesh * mesh = uh->fes->mesh();
    for(CellIterator c(*mesh); !c.end(); ++c){
        vector<int> idx = c->vertices();
        topostream << idx[0]; 
        for(int i = 1; i < (int) idx.size(); ++i){
            topostream << "," << idx[i];
        }
        topostream << endl;
    }
}



/**
 * Writes the vertex coordinates of the mesh
 * 
 **/
void DataWriter::writeVertexCoords(){
    Mesh * mesh = uh->fes->mesh();
    for(VertexIterator v(*mesh); !v.end(); ++v){
        Point x = v->point();
        coordstream << x(0); 
        for(int i = 1; i < (int) x.numComponents(); ++i){
            coordstream << "," << x(i);
        }
        coordstream << endl;
    }
}



/**
 * Writes the field data at the verticies
 * 
 * 
 **/
void DataWriter::writeScalarField(){
    FiniteElementSpace *fes = uh->fes;
    for(int i = 0; i < fes->unconstrainedSize(); i++){
        fieldstream << (*uh)[fes->ID[i]] << endl;
    }
}



/**
 * Writes all the data associated with this FiniteElementFunction
 * 
 **/
void DataWriter::writeAllData(){
    writeElementTopology();
    writeVertexCoords();
    writeScalarField();
}