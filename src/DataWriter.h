#ifndef DataWriter_h
#define DataWriter_h
#include <iostream>
#include "FiniteElementFunction.h"

namespace fem{

    class DataWriter{
    protected:
        std::ofstream topostream, coordstream, fieldstream;
        FiniteElementFunction *uh;

    public:
        DataWriter(std::string dir, FiniteElementFunction &u);
        void writeElementTopology();
        void writeVertexCoords();
        void writeScalarField();
        void writeAllData();

    };

}

#endif