#include "DirichletBC.h"

using namespace fem;
using namespace std;
using namespace Eigen;


/**
 * Constructs a DirichletBC on a FiniteElementSpace over some Facet MeshTag
 * 
 * @param f function to apply
 * @param s FiniteElementSpace
 * @param t MeshTag value for the boundary facets to constrain
 * 
 **/
DirichletBC::DirichletBC(ScalarFunction &f, FiniteElementSpace &s, int t)
:   fes(&s),
    tag(t),
    g(&f)
{}


/**
 * Applies to bc to a FiniteElementFunction
 * 
 * @param f FiniteElementFunction to be applied to
 * 
 **/
void DirichletBC::apply(FiniteElementFunction &u){
    // loop through tagged faces and evaluate the function on the closure
    for(FacetIterator f(*fes->mesh(), tag); !f.end(); ++f){
        ///@todo fill this in
    }
}