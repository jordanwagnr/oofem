#ifndef DirichletBC_h
#define DirichletBC_h
#include "FiniteElementFunction.h"
#include "Expression.h"

namespace fem{

    class DirichletBC{
    protected:
        FiniteElementSpace *fes;
        int tag;
        ScalarFunction *g;

    public:
        DirichletBC(ScalarFunction &g, FiniteElementSpace &s, int t);

        void apply(FiniteElementFunction &f);

    };

}

#endif