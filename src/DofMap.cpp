#include "DofMap.h"
#include "FiniteElementSpace.h"


using namespace fem;
using namespace std;



/**
 * Builds the DofMap based on the FiniteElementSpace. This is basically a PetscSection. Right now, 
 * this would only work for up to two dimensions, I think.
 * 
 * @param s FiniteElementSpace
 * 
 **/
void DofMap::build(FiniteElementSpace &s){
    // allocate arrays
    _offsets.resize(s.dim+1);
    _ndofs.resize(s.dim+1);
    for(int i = 0; i <= s.dim; i++){
        _offsets[s.dim-i].resize((s.mesh()->numEntities())[i]);
        _ndofs[s.dim-i].resize(s.mesh()->numEntities()[i]);
    }

    // used space info to build the map
    // start with vertices
    int nnv = s.fe->nodeLayout()[0];
    int off = 0;
    for(VertexIterator v(*s.mesh()); !v.end(); ++v){
        _ndofs[s.dim][v->index()] = nnv;
        _offsets[s.dim][v->index()] = off;
        off += nnv; 
    }

    // next do facets
    int nnf = s.fe->nodeLayout()[s.dim-1];
    for(FacetIterator f(*s.mesh()); !f.end(); ++f){
        _ndofs[1][f->index()] = nnf;
        _offsets[1][f->index()] = off;
        off += nnf;
    }

    // now do cells
    int nnc = s.fe->nodeLayout()[s.dim];
    for(CellIterator c(*s.mesh()); !c.end(); ++c){
        _ndofs[0][c->index()] = nnc;
        _offsets[0][c->index()] = off;
        off += nnc;
    }
}



/**
 * Returns the number of dofs attached to a given Cell
 * 
 * @param c Cell
 * 
 **/
int DofMap::numDof(Cell &c){
    return _ndofs[0][c.index()];
}



/**
 * Returns the offset into the global dofs index set that corresponds to given Cell
 * 
 * @param c Cell
 * 
 **/
int DofMap::offset(Cell &c){
    return _offsets[0][c.index()];
}



/**
 * Returns the number of dofs attached to a given Facet
 * 
 * @param f Facet
 * 
 **/
int DofMap::numDof(Facet &f){
    return _ndofs[1][f.index()];
}



/**
 * Returns the offset into the global dofs index set that corresponds to given Facet
 * 
 * @param f Facet
 * 
 **/
int DofMap::offset(Facet &f){
    return _offsets[1][f.index()];
}




/**
 * Returns the number of dofs attached to a given Vertex
 * 
 * @param v Vertex
 * 
 **/
int DofMap::numDof(Vertex &v){
    return _ndofs[_ndofs.size()-1][v.index()];
}



/**
 * Returns the offset into the global dofs index set that corresponds to given Vertex
 * 
 * @param v Vertex
 * 
 **/
int DofMap::offset(Vertex &v){
    return _offsets[_ndofs.size()-1][v.index()];
}