#ifndef DofMap_h
#define DofMap_h
#include <vector>
#include "MeshEntity.h"

namespace fem{
    class FiniteElementSpace;

    class DofMap{
    protected:
        std::vector< std::vector<int> > _offsets;
        std::vector< std::vector<int> > _ndofs;

    public:
        DofMap(){};

        void build(FiniteElementSpace &s);
        
        int numDof(Cell &c);
        int offset(Cell &c);
        int numDof(Facet &f);
        int offset(Facet &f);
        int numDof(Vertex &v);
        int offset(Vertex &v);

    };
}

#endif