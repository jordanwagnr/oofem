#include "Expression.h"
#include "common.h"

using namespace fem;
using namespace std;

/**
 * Constructs a constant expression
 * 
 * @param c constant value
 * 
 **/
Constant::Constant(double c)
:   constant(c)
{}



/**
 * Returns the function value
 * 
 * @param Point point to evaluate the function
 * 
 **/
double Constant::evaluate(Point &x){
    return constant;
}




/**
 * Constructs a cosine function, Acos(wx) = Acos(wx) + Acos(wy)
 * 
 * @param a amplitude
 * @param w 
 * 
 **/
Cosine::Cosine(double a, double w)
:   A(a), omega(w)
{}



/**
 * Returns the function value
 * 
 * @param Point to evaluate
 * 
 **/
double Cosine::evaluate(Point &x){
    double val = 0;
    for(int i = 0; i < x.numComponents(); i++){
        val += A*cos(omega*x(i));
    }
    return val;
}



/**
 * Constucts a Weierstrass-Mandebrot fractal
 * 
 * @param v variance
 * @param d fractal dimension
 * 
 **/
WMFractal::WMFractal(double v, double d)
:   var(v),
    D(d)
{
    // compute 1D fractal dimension
    D1 = D-1;
    // compute G from assumed power spectra
    G = pow((4*v*log(gamma)*(4-2*D1)) / ( 1/pow(w1,4-2*D1) - 1/pow(w2,4-2*D1)), 1/(2*D1-2));
    // compute max frequency index
    nmax = floor(log10(L/Ls)/log10(gamma)) + 1;
    // compute invariant amplitude
    A = L*pow( (G/L), D-2 ) * sqrt(log(gamma)/M); 
    // generate random phases between 0 and 2pi
    phi.resize(M+1);
    for(int m = 1; m <= M; m++){
        phi[m].resize(nmax+1);
        for(int n = 1; n <= nmax; n++){
            phi[m][n] = ((double) rand() / (RAND_MAX));
        }
    }
}



/**
 * Computes the truncated WM fractal value at a Point
 * 
 **/
double WMFractal::evaluate(Point &x){
    double theta = x.norm() > 10e-9 ? theta = atan2(x(1),x(0)): theta = 0;  
    double sum = 0;
    // loop over angles
    for(int m = 1; m <= M; m++){
        // loop over frequencies
        for(int n = 1; n <= nmax; n++){
            sum += pow(gamma, D*n-3*n)*
                ( cos(2*pi*phi[m][n]) - 
                    cos((2*pi/L)*pow(gamma,n)*(x.norm())*
                        cos(theta-pi*m/M) + 2*pi*phi[m][n]));
        }
    }
    return A*sum + .5;
}
