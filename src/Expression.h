#ifndef Expression_h
#define Expression_h
#include "Point.h"


namespace fem{

    template <class T>
    class Expression{
    protected:

    public:
        Expression(){};

        virtual T evaluate(Point &x) = 0;
        T operator()(Point &x){
            return evaluate(x);
        }
    };


    class Constant: public Expression<double>{
    protected:
        double constant;

    public:
        Constant(double c = 0);
        double evaluate(Point &x);
        

    };


    class Cosine: public Expression<double>{
    protected:
        double A;
        double omega;

    public:
        Cosine(double A = 1, double w = 2*M_PI);
        double evaluate(Point &x);

    };



    class WMFractal: public Expression<double>{
    protected:
        double var;
        double D;
        double G;
        double D1;
        double A;
        int nmax;
        int M = 20;
        double gamma = 1.5;
        double L = 1.;
        double Ls = 5e-6;
        double w1 = 1; 
        double w2 = 100;
        std::vector<std::vector<double>> phi;

    public:
        WMFractal(double v, double D);

        double evaluate(Point &x);
    };


    typedef Expression<double> ScalarFunction;

}

#endif