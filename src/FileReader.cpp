#include "FileReader.h"
#include <algorithm>

using namespace std;
using namespace Eigen;
using namespace fem;

/**
 * Opens a FileReader associated with a given file
 * 
 * @param f filename
 * 
 **/
FileReader::FileReader(string f){
     fstr.open(f);
}


/**
 * Reads the next line of an open file. If the line is commented "//", the line is ignored. Leading
 * and trailing whitespaces are stripped.
 *
 **/
string FileReader::readLine(){
    string s;
    string::size_type i;

    if(fstr.good()){
        if(atEof()){
           return s; 
        }

        getline(fstr, s);

        // Erase the string if it is a comment
        i = s.find_first_of("//");
        if(i != string::npos){
            s.erase(s.begin()+i, s.end());
        }
        // Erase leading spaces
        i = s.find_first_not_of(" ");
        if(i > 0 && i != string::npos){
            s.erase(s.begin(), s.begin()+i);
        }
        // Erase tab only lines
        i = s.find_first_not_of("\t");
        if(i == string::npos){
            i = s.find_first_of("\t");
            if(i == 0){
                s.erase();
            }
        }
        // Erase trailing spaces
        i = s.find_last_not_of(" ");
        if(i != string::npos){
            s.erase(s.begin()+i+1,s.end());
        }

    }
    else{
        throw "readLine error.";
    }

    return s;
}



/**
 * Reads next line that is not blank or commented.
 * 
 **/
string FileReader::readNotBlank(){
    string s;
    s = readLine();
    while(!s.compare("") && !atEof()){
        s = readLine();
    }
    return s;
}


/**
 * Checks whether or not the reader is at the end of file.
 * 
 **/
bool FileReader::atEof(){
    return fstr.eof();
}


/**
 * Moves reader to beginning of file.
 * 
 **/
void FileReader::home(){
    fstr.clear();
    fstr.seekg(0,ios::beg);
}


/**
 * Moves reader to end of file.
 * 
 **/
void FileReader::end(){
    fstr.seekg(0,ios::end);
}


/**
 * Constructs a new GmshReader capable of reading a GMSH mesh file.
 * 
 * @param 
 * 
 **/
GmshReader::GmshReader(Mesh *m, string f)
:   FileReader(f),
    msh(m)
{}



/**
 * Searches the .msh file for a section denoted by "$", e.g. $PhysicalNames., and sets
 * the FileReader at this positon. The postition relative the beginning is returned. 
 * 
 * @param s section name without the dollar sign included
 * 
 **/
int GmshReader::goToSection(string s){
    home();
    string buf = readNotBlank();
    while(buf != "$"+s ){
        buf = readNotBlank();
    }
    return fstr.tellg();
}

/**
 * Reads the line surrounded by $MeshFormat / $EndMeshFormat
 * 
 **/
string GmshReader::readMeshFormat(){
    goToSection("MeshFormat");
    return readNotBlank(); 
}


/**
 * Reads the number of nodes.
 * 
 **/
int GmshReader::readNumVertices(){
    goToSection("Nodes");
    int nv = stoi(readNotBlank());
    if(msh){
        msh->numverts = nv;
    }
    return nv;
}


/**
 * Reads the number of elements
 * 
 **/
int GmshReader::readNumElmts(){
    goToSection("Elements");
    int ne = stoi(readNotBlank());
    if(msh){
        msh->numelmts = ne;
    }
    return ne;
}


/**
 * Reads the element types. Returns an std::map where the key is the GMSH integer id for the element
 * type and the value is the number of elements of that type
 * 
 **/
map<int, int> GmshReader::readElmtTypes(){
    int nel = readNumElmts();  // this will put the stream in the correct position
    string buf;
    stringstream sstr;
    // loop through elements to determine the number of cells and facets
    ArrayXi eltype(nel); 
    int eplus1;
    map<int, int> netype;
    for(int e = 0; e < nel; e++){
        buf = readLine();
        sstr.clear();
        sstr.str(buf);
        sstr >> eplus1 >> eltype(e);
        netype[eltype(e)] ++;
    }
    return netype;
}



/**
 * Reads the vertex (i.e. node) data from the gmsh file.
 * 
 **/
void GmshReader::readVertices(){
    int numvert = readNumVertices(); // this will put reader in correct position
    string buf;
    stringstream sstr;
    // Gmsh always lists 3 coordinates no matter what dimension the mesh is. However, it the elmts
    // have already been read through, we no what dimension the mesh really is.
    if(msh->dim){
        msh->_coordinates.resize(numvert*msh->dim);    
    }
    else{
        msh->_coordinates.resize(numvert*3);  // gmsh always give 3 coordinates no matter the dim
    }
    int vplus1;
    if(msh->dim == 1){
        for(int v = 0; v < numvert; v++){
            buf = readLine();
            sstr.clear();
            sstr.str(buf);
            sstr >> vplus1 >> 
            msh->_coordinates(v*msh->dim);
            assert(vplus1 == v+1);
        }
    }
    if(msh->dim == 2){
        for(int v = 0; v < numvert; v++){
            buf = readLine();
            sstr.clear();
            sstr.str(buf);
            sstr >> vplus1 >> 
                msh->_coordinates(v*msh->dim) >>
                msh->_coordinates(v*msh->dim + 1);
            assert(vplus1 == v+1);
        }
    }
    else{
        for(int v = 0; v < numvert; v++){
            buf = readLine();
            sstr.clear();
            sstr.str(buf);
            sstr >> vplus1 >> 
                msh->_coordinates(v*msh->dim) >>
                msh->_coordinates(v*msh->dim + 1) >>
                msh->_coordinates(v*msh->dim + 2);
            assert(vplus1 == v+1);
        }
    }
}


/**
 * Reads the element data and returns an array containing all the information according to GMSH file
 * format. see http://gmsh.info/doc/texinfo/gmsh.html#MSH-file-format-version-2-_0028Legacy_0029
 *
 **/
void GmshReader::readElmts(){
    // first check validity of the data
    // only types 1,2,3,15 are allowed
    vector<int> notvalid = {4,5,6,7,8,9,10,11,12,13,14,16,17,18,19};
    map<int,int> ntypes = readElmtTypes(); 
    for(int t = 0;  t < (int) notvalid.size(); t++){
        if(ntypes.count(notvalid[t])){
            throw "Gmsh file contains invalid element types";
        }
    }
    // determine dimension, and number of cells and facets from the types
    // also set the total number of vertices that will be contained in the cell and facet _topology
    // arrays.
    int nc, nf;
    int ntri = ntypes[2];    // number of tri elements
    int nquad = ntypes[3];   // number of quad elements
    int nline = ntypes[1];   // number of line elements
    int npoint = ntypes[15];  // number of point elements
    vector<int> nv; // number of vertices per codimension element
    if(ntri + nquad){
        msh->dim = 2;
        nc = ntri + nquad;
        nf = nline;
        int ncv = ntri*3 + nquad*4;
        int nfv = nline*2;
        nv.resize(3);
        nv[0] = ncv;
        nv[1] = nfv;
        nv[2] = npoint;
    }
    else{
        msh->dim = 1;
        nc = nline;
        nf = npoint;
        int ncv = nline*2;
        int nfv = npoint;
        nv.resize(2);
        nv[0] = ncv;
        nv[1] = nfv;
    }

    // resize everything and read the cells and facets
    msh->_topology.resize(msh->dim+1);
    msh->_offsets.resize(msh->dim+1);
    msh->nve.resize(msh->dim+1);
    msh->types.resize(msh->dim+1);
    msh->tagregister.resize(msh->dim+1);
    for(int k = 0; k < msh->dim; k++){
        msh->_topology[k].resize(nv[k]);
    }
    msh->numcells = nc;
    msh->numfacets = nf;
    msh->numverts = readNumVertices();
    msh->_offsets[0].resize(msh->numcells);
    msh->_offsets[1].resize(msh->numfacets);
    msh->nve[0].resize(msh->numcells);
    msh->nve[1].resize(msh->numfacets);
    msh->types[0].resize(msh->numcells);
    msh->types[1].resize(msh->numfacets);
    readCells(2);
    readFacets(2);
}



/**
 * Reads the cell information given the mesh dimension. The physical tag for each element is read
 * and added to the Mesh obect.
 *
 * @param int d geometric dimension of the mesh
 *
 **/
void GmshReader::readCells(int d){
    int nel = readNumElmts(); 
    string buf;
    stringstream sstr;
    int type, ntags, ignore, off = 0, cidx = 0;
    map<int, vector<int>> tags;
    
    for(int e = 0; e < nel; e++){
        buf = readLine();
        sstr.clear();
        sstr.str(buf);
        sstr >> ignore >> type >> ntags;
        assert(ntags > 1);
        // store the required ptag but ignore the rest
        int ptag;
        sstr >> ptag;
        for(int t = 1; t < ntags; t++){
            sstr >> ignore;
        }
        if( d == 2){
            switch(type){
                case 2:{
                    msh->_offsets[0][cidx] = off;
                    msh->nve[0][cidx] = 3;
                    msh->types[0][cidx] = type;
                    tags[ptag].push_back(cidx);
                    cidx++;
                    // gmsh triangles are inverted, they use cw ordering 
                    sstr >> 
                        msh->_topology[0][off+2] >>
                        msh->_topology[0][off+1] >>
                        msh->_topology[0][off];
                    off += 3;
                    break;
                }
                case 3:{
                    msh->_offsets[0][cidx] = off;
                    msh->nve[0][cidx] = 4;
                    msh->types[0][cidx] = type;
                    tags[ptag].push_back(cidx);
                    cidx++;
                    // gmsh quads are inverted, they use cw ordering
                    sstr >> 
                        msh->_topology[0][off+3] >> 
                        msh->_topology[0][off+2] >>
                        msh->_topology[0][off+1] >>
                        msh->_topology[0][off];
                    off += 4;
                    break;
                }
            }
        }
        if( d == 1){
            switch(type){
                case 1:{
                    msh->_offsets[0][cidx] = off;
                    msh->nve[0][cidx] = 2;
                    msh->types[0][cidx] = type;
                    tags[ptag].push_back(cidx);
                    cidx++;
                    sstr >>
                        msh->_topology[0][off] >>
                        msh->_topology[0][off+1];
                    off += 2;
                    break;
                }
            }
        }
    }

    // process the recorded tags
    for(map<int, vector<int>>::iterator it = tags.begin(); it != tags.end(); it++){
        msh->createTag(it->first, msh->dim, it->second);
    }

    // since gmsh uses 1 based indexing, we have to subtract 1 from every index in the topology
    for(int &i : msh->_topology[0]){ // quick way to subtract 1 from every entry
        --i;
    }
}



/**
 * Reads the facet information given the mesh dimension
 * 
 * @param d mesh dimension (not facet dimension)
 * 
 **/
void GmshReader::readFacets(int d){
    int nel = readNumElmts(); 
    string buf;
    stringstream sstr;
    int type, ntags, ignore, off = 0, fidx = 0;
    map<int, vector<int>> tags;
    
    for(int e = 0; e < nel; e++){
        buf = readLine();
        sstr.clear();
        sstr.str(buf);
        sstr >> ignore >> type >> ntags;
        assert(ntags > 1);
        // store the required ptag but ignore the rest
        int ptag;
        sstr >> ptag;
        for(int t = 1; t < ntags; t++){
            sstr >> ignore;
        }
        if( d == 2){
            switch(type){
                case 1:{
                    msh->_offsets[1][fidx] = off;
                    msh->nve[1][fidx] = 2;
                    msh->types[1][fidx] = type;
                    tags[ptag].push_back(fidx);
                    fidx++;
                    sstr >> 
                        msh->_topology[1][off] >>
                        msh->_topology[1][off+1];
                    off += 2;
                    break;
                }
            }
        }
        if(d == 1){
            switch(type){
                case 15:{
                    msh->_offsets[1][fidx] = off;
                    msh->nve[1][fidx] = 1;
                    msh->types[1][fidx] = type;
                    tags[ptag].push_back(fidx);
                    fidx++;
                    sstr >>msh->_topology[1][off];
                    off ++;
                    break;
                }
            }
        }
    }

    // process the recorded tags
    for(map<int, vector<int>>::iterator it = tags.begin(); it != tags.end(); it++){
        msh->createTag(it->first, msh->dim-1, it->second);
    }

    // since gmsh uses 1 based indexing, w  e have to subtract 1 from every index in the topology
    for(int &i : msh->_topology[1]){ // quick way to subtract 1 from every entry
        --i;
    }
}


