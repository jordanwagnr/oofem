#ifndef FileReader_h
#define FileReader_h
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <Eigen/Dense>
#include "Mesh.h"
#include <map>

namespace fem{
    class Mesh;
    /**
     * Wraps the std::fstream class to provide basic file parsing functions.
     * 
     **/
    class FileReader{
    protected:
        enum ELMT_t {TRIA = 2, LINE = 1};
        std::ifstream fstr;
        
    public:
        FileReader(){};
        FileReader(std::string);
        
        std::string readLine();
        std::string readNotBlank();
        void home();
        void end();
        bool atEof();
        
    };


    /**
     * Provides basic functions for reading GMSH (i.e. .msh) files.
     * 
     **/
    class GmshReader: public FileReader{
    protected:
        Mesh *msh;

    public:
        GmshReader(){};
        GmshReader(Mesh *m, std::string);

        std::string readMeshFormat();
        int goToSection(std::string);
        int readNumVertices();
        int readNumElmts();
        std::map<int,int> readElmtTypes();
        void readVertices();
        void readElmts();
        void readCells(int d);
        void readFacets(int d);
    };
}

#endif