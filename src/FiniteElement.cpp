#include "FiniteElement.h"

using namespace fem;
using namespace std;
using namespace Eigen;


/**
 * Creates a FiniteElement base class
 * 
 * @param c geometry type of the reference element
 * 
 **/
FiniteElement::FiniteElement(GEOM_t c)
:   refgeom(c)
{
    switch (c){
        case LINE:{
            dim = 1;
            nverts = 2;
            break;
        }
        case TRIA:{
            dim = 2;
            nverts = 3;
            break;
        }
        case QUAD:{
            dim = 2;
            nverts = 4;
            break;
        }
        default: cout << "Invalid geometry type in FiniteElement construction." << endl;
    }
    nodelayout.resize(dim+1);
}



/**
 * Returns the coordinates of the nodes (dofs) in the element
 * 
 **/
vector<Point> & FiniteElement::nodePoints(){
    return nodecoords;
}



/**
 * Returns the number nodes (dofs) in the element
 * 
 **/
int FiniteElement::numNodes(){
    return nnodes;
}



/**
 * Returns the nodelayout, which is a vector specifying the number of nodes (dofs) attached to each
 * dimensional entity of the finite element. For example, a quadratic triangle element would have a
 * node layout of (1,1,0), which means there is 1 dof on every vertex (0-D), 1 dof on every edge
 * (1-D), and 0 dofs on the cell itself (2-d).
 *
 **/
vector<int> FiniteElement::nodeLayout(){
    return nodelayout;
}



/**
 * Tabulates the basis functions evaluated at each point of a quadrature rule
 * 
 * @param Quadrature rule
 * 
 **/
vector<VectorXd> FiniteElement::tabulateBasisValues(Quadrature &q){
    vector<Point> qp = q.points();
    vector<VectorXd> tab(qp.size());
    for(int i = 0; i < (int) qp.size(); i++){
        tab[i] = shapeValues(qp[i]);
    }
    return tab;
}



/**
 * Tabulates the basis derivatives evaluated at each point of a quadrature rule
 * 
 * @param Quadrature rule
 * 
 **/
vector<MatrixXd> FiniteElement::tabulateBasisDerivatives(Quadrature &q){
    vector<Point> qp = q.points();
    vector<MatrixXd> tab(qp.size());
    for(int i = 0; i < (int) qp.size(); i++){
        tab[i] = shapeDerivatives(qp[i]);
    }
    return tab;
}





/**
 * Creates a LagrangeFiniteElement over a specified reference element
 *  
 * @param c geometry type of the reference element
 * @param o order of the element (default is o = 1 (linear))
 * 
 **/
LagrangeFiniteElement::LagrangeFiniteElement(GEOM_t c, int o)
:   FiniteElement(c),
    order(o)
{
    switch(c){
        case LINE:  setNodesLine_internal(); break;
        case TRIA:  setNodesTria_internal(); break;
        case QUAD:  setNodesQuad_internal(); break;
        default: cout << endl << 
            "Invalid order for LagrangeFiniteElement construction." << endl << endl; 
    }
}



/**
 * Returns the reference shape functions evaluated at the Point x
 * 
 * @param x Point in the reference element
 * 
 **/
VectorXd LagrangeFiniteElement::shapeValues(Point &x){
    assert(x.numComponents() == dim);
    VectorXd vals(nnodes);
    switch(refgeom){
        case LINE: return shapesLine_internal(x(0));
        case TRIA: return shapesTria_internal(x());
        case QUAD: return shapesQuad_internal(x());
        default: throw "Reference element not implemented.";
    }
}



/**
 * Returns the reference shape function derivatives evaluated at the Point x
 * 
 * @param x Point in the reference element
 * 
 **/
MatrixXd LagrangeFiniteElement::shapeDerivatives(Point &x){
    assert(x.numComponents() == dim);
    MatrixXd vals(nnodes, dim);
    switch(refgeom){
        case LINE: return derivsLine_internal(x(0));
        case TRIA: return derivsTria_internal(x());
        case QUAD: return derivsQuad_internal(x());
        default: throw "Reference element not implemented.";
    }
}




VectorXd LagrangeFiniteElement::shapesLine_internal(double x){
    VectorXd vals(nnodes);
    switch(order){
        case 1:{
            vals(0) =  .5*(1. - x);
            vals(1) =  .5*(1. + x);
            break;
        }
        default: throw "Element order not implemented.";
    }
    return vals;   
}



VectorXd LagrangeFiniteElement::shapesTria_internal(Ref<VectorXd> x){
    VectorXd vals(nnodes);
    switch(order){
        case 1:{
            vals(0) =  1. - x(0) - x(1);
            vals(1) =  x(0);
            vals(2) =  x(1);
            break;
        }
        default: throw "Element order not implemented.";
    }
    return vals;
}



VectorXd LagrangeFiniteElement::shapesQuad_internal(Ref<VectorXd> x){
    VectorXd vals(nnodes);
    switch(order){
        case 1:{
            vals(0) =  .25*(1. - x(0))*(1. - x(1));
            vals(1) =  .25*(1. + x(0))*(1. - x(1));
            vals(2) =  .25*(1. + x(0))*(1. + x(1));
            vals(3) =  .25*(1. - x(0))*(1. + x(1));
            break;
        }
        default: throw "Element order not implemented.";
    }
    return vals;
}




MatrixXd LagrangeFiniteElement::derivsLine_internal(double x){
    MatrixXd vals(nnodes, dim);
    switch(order){
        case 1:{
            vals(0) = -.5;
            vals(1) =  .5;
            break;
        }
        default: throw "Element order not implemented.";
    }
    return vals;
}



MatrixXd LagrangeFiniteElement::derivsTria_internal(Ref<VectorXd> x){
    MatrixXd vals(nnodes, dim);
    switch(order){
        case 1:{
            vals.row(0) << -1, -1;
            vals.row(1) <<  1,  0;
            vals.row(2) <<  0,  1;
            break;
        }
        default: throw "Element order not implemented.";
    }
    return vals;
}



MatrixXd LagrangeFiniteElement::derivsQuad_internal(Ref<VectorXd> x){
    MatrixXd vals(nnodes, dim);
    switch(order){
        case 1:{
            vals.row(0) << -0.25*(1-x(1)), -.25*(1-x(0));
            vals.row(1) <<  0.25*(1-x(1)), -.25*(1+x(0));
            vals.row(2) <<  0.25*(1+x(1)),  .25*(1+x(0));
            vals.row(3) << -0.25*(1+x(1)),  .25*(1-x(0));
            break;
        }
        default: throw "Element order not implemented.";
    }
    return vals;
}




void LagrangeFiniteElement::setNodesLine_internal(){
    switch(order){
        case 1:{
            nnodes = 2;
            nodelayout[0] = 1;   // 1 node per 0-d mesh entity 
            nodecoords.push_back(Point(-1.));
            nodecoords.push_back(Point(1.));
            break;
        }
        default: throw "Element order not implemented.";
    }   
}



void LagrangeFiniteElement::setNodesTria_internal(){
    switch(order){
        case 1:{
            nnodes = 3;
            nodelayout[0] = 1;  // 1 node per 0-d mesh entity
            nodecoords.push_back(Point(0.,0.));
            nodecoords.push_back(Point(1.,0.));
            nodecoords.push_back(Point(0.,1.));
            break;
        }
        default: throw "Element order not implemented.";
    }   
}



void LagrangeFiniteElement::setNodesQuad_internal(){
    switch(order){
        case 1:{
            nnodes = 4;
            nodelayout[0] = 1; // 1 node per 0-d mesh entity
            nodecoords.push_back(Point( -1., -1. ));
            nodecoords.push_back(Point(  1., -1. ));
            nodecoords.push_back(Point(  1.,  1. ));
            nodecoords.push_back(Point( -1.,  1. ));
            break;
        }
        default: throw "Element order not implemented.";
    }   
}