#ifndef FiniteElement_h
#define FiniteElement_h
#include "Point.h"
#include "common.h"
#include "Quadrature.h"


namespace fem{

    class FiniteElement{
    protected:
        int nnodes, dim, nverts;        
        GEOM_t refgeom;
        std::vector<Point> nodecoords;
        std::vector<int> nodelayout;

    public:
        FiniteElement(GEOM_t c);

        virtual Eigen::VectorXd shapeValues(Point &x) = 0;
        virtual Eigen::MatrixXd shapeDerivatives(Point &x) = 0;
        std::vector<Point> & nodePoints();
        int numNodes();
        std::vector<int> nodeLayout();
        std::vector<Eigen::VectorXd> tabulateBasisValues(Quadrature &q);
        std::vector<Eigen::MatrixXd> tabulateBasisDerivatives(Quadrature &q);
        
    };


    class LagrangeFiniteElement: public FiniteElement{
    protected:
        int order;

        Eigen::VectorXd shapesLine_internal(double x);
        Eigen::VectorXd shapesTria_internal(Eigen::Ref<Eigen::VectorXd> x);
        Eigen::VectorXd shapesQuad_internal(Eigen::Ref<Eigen::VectorXd> x);
        Eigen::MatrixXd derivsLine_internal(double x);
        Eigen::MatrixXd derivsTria_internal(Eigen::Ref<Eigen::VectorXd> x);
        Eigen::MatrixXd derivsQuad_internal(Eigen::Ref<Eigen::VectorXd> x);
        void setNodesLine_internal();
        void setNodesTria_internal();
        void setNodesQuad_internal();

    public:
        LagrangeFiniteElement(GEOM_t, int o = 1);

        Eigen::VectorXd shapeValues(Point &x);
        Eigen::MatrixXd shapeDerivatives(Point &x);


    };
}

#endif