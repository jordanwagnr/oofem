#include "FiniteElementFunction.h"

using namespace fem;
using namespace std;
using namespace Eigen;


/**
 * Creates a FiniteElementFunction in a FiniteElementSpace. By default, the function does not 
 * include its constrained values.
 * 
 * @param s FiniteElementSpace where the function lives
 * @param c whether or not the function is constrained
 * 
 **/
FiniteElementFunction::FiniteElementFunction(FiniteElementSpace &s, bool c)
:   fes(&s)
{
    if(c){
        u.resize(s.constrainedSize());
        u << VectorXd::Zero(s.constrainedSize());
        uc.resize(s.numConstraints());
        uc << VectorXd::Zero(s.numConstraints());
    }
    else{
        u.resize(s.unconstrainedSize());
        u << VectorXd::Zero(s.unconstrainedSize());
    }
}



/**
 * Accesses a reference to the ith coefficient of the function. If i is negative, then the operation
 * accesses the (i-1) element of the constrained 
 *
 * @param i index of coefficient to access
 *
 **/
double & FiniteElementFunction::operator[](int i){
    if(i >= 0){
        return u(i);
    }
    else{
        return uc(-(i+1));
    }
}



/**
 * Returns the pullback of the function from a MeshEntity to the reference element 
 * 
 * @param x reference coordinate
 * @param e MeshEntity
 * 
 **/
double FiniteElementFunction::operator()(Point &x, MeshEntity &e){
    vector<int> closure = e.vertices();
    VectorXd N = fes->fe->shapeValues(x);
    VectorXd dofs(fes->fe->numNodes());
    int i = 0;
    for(VertexIterator v(*fes->_mesh, closure); !v.end(); ++v){
        int off = fes->dmap.offset(*v);
        int ndofs = fes->dmap.numDof(*v);
        for(int d = 0; d < (int) ndofs; d++){
            dofs(i) = (*this)[fes->ID[off+d]];
            i++;
        }
    }
    return dofs.transpose()*N;
}



/**
 * Returns the vector of coefficients of the function. This does not make a copy. These are the dof
 * values.
 *
 **/
Map<VectorXd> FiniteElementFunction::coeffs(){
    Map<VectorXd> um(u.data(), u.size());
    return um;
}



/**
 * Returns the vector of constrained coefficients of the function. This does not make a copy. These
 * are the dof values.
 *
 **/
Map<VectorXd> FiniteElementFunction::coeffsConstrained(){
    Map<VectorXd> um(uc.data(), uc.size());
    return um;
}




/**
 * Returns whether the function is constrained or not.
 * 
 **/
bool FiniteElementFunction::constrained(){
    return _constrained;
}


/**
 * Returns the size of the function, i.e. the number of dofs contained
 * 
 **/
int FiniteElementFunction::size(){
    return u.size();
}


/**
 * Restricts the function to a MeshEntity in the mesh.
 * 
 * @warning This function will only work for linear, Lagrange elements. Needs a full closure 
 * operation.
 * 
 * @param e MeshEntity
 * 
 **/
VectorXd FiniteElementFunction::restrict(MeshEntity &e){
    VectorXd uloc(fes->fe->numNodes());
    // loop through closure of c and get associated dofs
    vector<int> closure = e.vertices(); ///@note Not the real closure!
    ///@note it would be nice to have something like a ClosureIterator
    int i = 0;
    for(VertexIterator v(*fes->mesh(),closure); !v.end(); ++v){
        int off = fes->dmap.offset(*v);
        int ndof = fes->dmap.numDof(*v);
        for(int d = 0; d < ndof; d++){
            uloc[i] = (*this)[fes->ID[off+d]];    
            i++;
        }
    }
    return uloc;
}



/**
 * Scatters the local vector over a MeshEntity to the global vector by addition.
 * 
 * @param e MeshEntity
 * @param uloc restriction
 * 
 **/
void FiniteElementFunction::add(MeshEntity &e, Ref<VectorXd> uloc){
    vector<int> dofs = fes->closureDofs(e);
    for(int d = 0; d < (int) dofs.size(); d++){
        (*this)[fes->ID[dofs[d]]] += uloc[d];
    }
}



/**
 * Inserts the constrained values in the proper locations and returns the full vector of solution
 * coefficients
 * 
 **/
VectorXd FiniteElementFunction::insertConstraints(){
    VectorXd uh(fes->unconstrainedSize());
    for(int i = 0; i < fes->unconstrainedSize(); i++){
        uh(i) = (*this)[fes->ID[i]];
    }
    return uh;
}




/**
 * Returns the FiniteElementSpace associated with this function
 * 
 **/
FiniteElementSpace * FiniteElementFunction::space(){
    return fes;
}