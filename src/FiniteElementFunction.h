#ifndef FiniteElementFunction_h
#define FiniteElementFunction_h
#include "FiniteElementSpace.h"

namespace fem{

    class FiniteElementFunction{
    protected:
        friend class DirichletBC;
        friend class DataWriter;
        
        Eigen::VectorXd u;
        Eigen::VectorXd uc;
        FiniteElementSpace *fes;
        bool _constrained;

    public:
        FiniteElementFunction(){};
        FiniteElementFunction(FiniteElementSpace &s, bool c = true);

        double & operator[](int i);
        
        double  operator()(Point &x, MeshEntity &e);
        Eigen::Map<Eigen::VectorXd> coeffs();
        Eigen::Map<Eigen::VectorXd> coeffsConstrained();
        Eigen::VectorXd & vcoeffs(){return u;}
        bool constrained();
        int size();
        Eigen::VectorXd restrict(MeshEntity &e);
        void add(MeshEntity &e, Eigen::Ref<Eigen::VectorXd> uloc);
        Eigen::VectorXd insertConstraints();
        FiniteElementSpace * space();


    };


}


#endif