#include "FiniteElementSpace.h"

using namespace fem;
using namespace std;
using namespace Eigen;

/**
 * Constructs a FiniteElementSpace from a Mesh and an element type (currently, only LAGRANGE
 * first-order elements are implemented).
 *
 * @param m Mesh
 * @param t Element type (default is LAGRANGE)
 * @param o order of the space (default is 1)
 * 
 **/
FiniteElementSpace::FiniteElementSpace(Mesh &m, FE_t t, int o)
:   type(t),
    dim(m.dimension()),
    order(o),
    _mesh(&m)
{
    // create reference element and mapping
    fe = new LagrangeFiniteElement(_mesh->cellType(), order);
    phi = new IsoparametricMapping(*fe, *_mesh);

    // compute number of nodes based on the FiniteElement nodelayout
    gnodelayout.resize(dim+1);
    ngn = 0;
    for(int k = 0; k < dim + 1; k++){
        gnodelayout[k] = fe->nodeLayout()[k]*_mesh->numEntities()[k];
        ngn += gnodelayout[k];
    }

    dmap.build(*this); 
    buildIdArray();
}



/**
 * Mark a given set of global dofs as constained. This function can be applied multiple times, which
 * will update the constrained dofs with any that are not already there. The user is required to
 * rebuild the ID array with buildIdArray() after adding additional constraint dofs.  
 *
 * @param g a set of global dof indices to constrain
 *
 **/
void FiniteElementSpace::constrainDofs(set<int> &g){
    // add unique values to the set
    set_union(
        cdofs.begin(), cdofs.end(), 
        g.begin(), g.end(), 
        inserter(cdofs, cdofs.end())
        );
}




/**
 * Builds the destination array; i.e. the ID array. This is a map from the global dofs to the index
 * into a matrix or vector. Constrained indices are marked with negative values. This is where a new
 * type of Matrix ordering could be implemented.
 * 
 **/
void FiniteElementSpace::buildIdArray(){
    ID.clear();
    ID.resize(ngn);
    // loop through all the dofs in order
    int off = 0;
    int ccount = 0;
    for(int i = 0; i < ngn; i++){
        // check if the dof is constrained (this is very fast with std::set)
        if(cdofs.find(i) != cdofs.end()){
            // if constrained, mark with negative 
            ID[i] = -(ccount+1);
            ccount++;
        }
        else{
            ID[i] = off;
            off++;
        }
    }
}



/**
 * Returns a set of the global dofs attached to all entities in a MeshTag. Sets are always sorted
 * and unique.
 *
 * @warning Since I don't have a closure function for the MeshEntities, I can only only return
 * incident vertices. Thus, this only will get the nodes attached to incident vertices. i.e. this
 * only will work properly for linear, Lagrange elements.
 *
 * @todo Implement a closure function in mesh so that dofs over the whole entity closure can be
 * used.
 *
 * @param t MeshTag value
 * @param k dimension of the tagged entities
 *
 **/
set<int> FiniteElementSpace::getTaggedDofs(int t, int k){
    /// @note this is pretty ugly. I need to make a general entity iterator
    set<int> tdofs;
    if(k == _mesh->dimension()){
        for(CellIterator c(*_mesh, t); !c.end(); ++c){
            vector<int> closure = c->vertices(); // this isn't the whole closure!
            for(VertexIterator v(*_mesh, closure); !v.end(); ++v){
                int nd = dmap.numDof(*v);
                int off = dmap.offset(*v);
                for(int d = 0; d < nd; d++){
                    tdofs.insert(off+d);
                }
            }
        }
    }
    if(k == _mesh->dimension()-1){
        for(FacetIterator f(*_mesh, t); !f.end(); ++f){
            vector<int> closure = f->vertices(); // this isn't the whole closure!
            for(VertexIterator v(*_mesh, closure); !v.end(); ++v){
                int nd = dmap.numDof(*v);
                int off = dmap.offset(*v);
                for(int d = 0; d < nd; d++){
                    tdofs.insert(off+d);
                }
            }
        }
    }
    if(k == 0){
        for(VertexIterator v(*_mesh, t); !v.end(); ++v){
            vector<int> closure = v->vertices(); // this isn't the whole closure!
            for(VertexIterator v(*_mesh, closure); !v.end(); ++v){
                int nd = dmap.numDof(*v);
                int off = dmap.offset(*v);
                for(int d = 0; d < nd; d++){
                    tdofs.insert(off+d);
                }
            }
        }
    }
    
    
    return tdofs;
}




/**
 * Returns a Mesh pointer that this space is associated to
 * 
 **/
Mesh * FiniteElementSpace::mesh(){
    return _mesh;
}


/**
 * Returns the number of global associated to each dimensional mesh entity.
 * 
 **/
vector<int>  FiniteElementSpace::globalNodeLayout(){
    return gnodelayout;
}


/**
 * Returns a reference to the DofMap
 * 
 * 
 **/
DofMap * FiniteElementSpace::dofMap(){
    return &dmap;
}



/**
 * Returns the element mapping
 * 
 **/
IsoparametricMapping & FiniteElementSpace::mapping(){
    return *phi;
}



/**
 * Returns the size of a function in the space whose constrained values have been partitioned away.
 * 
 **/
int FiniteElementSpace::constrainedSize(){
    return ngn - cdofs.size();
}


/**
 * Returns the size of a function in the space whose constrained values are included
 * 
 **/
int FiniteElementSpace::unconstrainedSize(){
    return ngn;
}


/**
 * Returns the number of constrained dofs
 * 
 **/
int FiniteElementSpace::numConstraints(){
    return cdofs.size();
}



/**
 * Project a function into the space
 * 
 * @param f ScalarFunction to project 
 * 
 * @warning only works with linear, Lagrange elements.
 * 
 **/
FiniteElementFunction FiniteElementSpace::project(ScalarFunction &f){
    FiniteElementFunction fh(*this);
    // loop through cells and get closure
    for(CellIterator c(*_mesh); !c.end(); ++c){
        vector<int> closure = c->vertices(); ///@warning not the total closure
        // loop through the closure
        int i = 0;
        for(VertexIterator v(*_mesh, closure); !v.end(); ++v){
            // pushforward the nodes (i.e. apply functional sigma[f(phi(xi))])
            int ndof = dmap.numDof(*v);
            int off = dmap.offset(*v);
            for(int d = 0; d < ndof; d++){
                fh[ID[off+d]] = phi->pushForward(f, fe->nodePoints()[i], *c);
            }
            i++;
        }
    }
    return fh;
}



/**
 * Returns the reference FiniteElement that generates this space
 * 
 **/
FiniteElement & FiniteElementSpace::referenceElement(){
    return *fe;
}



/**
 * Returns a reference to the ID array
 * 
 **/
std::vector<int> * FiniteElementSpace::idArray(){
    return &ID;
}



/**
 * Returns the dof indices on the closure of a MeshEntity
 * 
 * @param e MeshEntity
 * 
 * @warning this only gets the vertex closures. this needs to be updated to return entire closure
 * so that higher order elements can be used
 * 
 **/
std::vector<int> FiniteElementSpace::closureDofs(MeshEntity &e){
    vector<int> closure = e.vertices();
    vector<int> dofs;
    dofs.reserve(fe->numNodes());
    for(VertexIterator v(*_mesh, closure); !v.end(); ++v){
        int ndofs = dmap.numDof(*v);
        int off = dmap.offset(*v);
        for(int d = 0; d < ndofs; d++){
            dofs.push_back(off+d);
        }
    }
    return dofs;
}