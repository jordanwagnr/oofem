#ifndef FiniteElementSpace_h
#define FiniteElementSpace_h
#include "FiniteElement.h"
#include "Mesh.h"
#include "DofMap.h"
#include "IsoparametricMapping.h"
#include <set>
#include "FiniteElementFunction.h"
#include "Expression.h"


namespace fem{

    class FiniteElementFunction;


    
    /**
     * This class implements a function space where FiniteElementFunctions live.
     * 
     **/
    class FiniteElementSpace{
    protected:
        friend class DofMap;
        friend class DirichletBC;
        friend class FiniteElementFunction;
        friend class LinearForm;
        friend class LinearFormIntegrator;
        friend class BilinearForm;
        friend class BilinearFormIntegrator;
        friend class DataWriter;


        FE_t type;
        int dim, order, ngn;       
        Mesh *_mesh;
        std::vector<int> gnodelayout;
        std::vector<int> ID;
        std::set<int> cdofs;
        FiniteElement *fe; 
        IsoparametricMapping *phi;
        DofMap dmap;

    public:
        FiniteElementSpace(){};
        FiniteElementSpace(Mesh &m, FE_t t = LAGRANGE, int o=1);

        
        std::set<int> getTaggedDofs(int t, int k);
        void constrainDofs(std::set<int> &g);
        void buildIdArray();
        Mesh * mesh();
        std::vector<int> globalNodeLayout();
        DofMap * dofMap();
        IsoparametricMapping & mapping();
        int constrainedSize();
        int unconstrainedSize();
        int numConstraints();
        FiniteElementFunction project(ScalarFunction &f);
        FiniteElement & referenceElement();
        std::vector<int> * idArray();
        std::vector<int> closureDofs(MeshEntity &e);


    };
}
#endif