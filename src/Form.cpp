#include "Form.h"

using namespace fem;
using namespace std;
using namespace Eigen;

LinearForm::LinearForm(FiniteElementSpace &s, bool c)
:   FiniteElementFunction(s,c)
{}



/**
 * Assembles the LinearForm by summing over all of the integrators
 * 
 **/
void LinearForm::assemble(){
    vector<int> dofs;
    VectorXd l;
    
    // use registered domain integrators to integrate over cells
    if(dlfi.size()){
        // begin cell loop
        for(CellIterator c(*fes->_mesh); !c.end(); ++c){
            // get dof indices on the closure of the cell
            dofs = fes->closureDofs(*c);
            // visit each registered integrator
            for(LinearFormIntegrator * I : dlfi){
                l = VectorXd::Zero(dofs.size());
                // integrate to get element vector
                I->integrate(*c, l);
                // accumulate into the global vector
                add(*c, l);
            }
        }
    }

}



/**
 * Adds an integrator over the domain
 * 
 * @param i LinearFormIntegrator
 * 
 **/
void LinearForm::addDomainIntegrator(LinearFormIntegrator *i){
    i->L = this;
    i->tabulateElement();
    dlfi.push_back(i);
}





/**
 * Constructs a BilinearForm with equal test and trial spaces
 * 
 * @param s FiniteElementSpace
 * 
 **/
BilinearForm::BilinearForm(FiniteElementSpace &s)
:   fes(&s)
{
    Amat.resize(fes->constrainedSize(), fes->constrainedSize());
    coeffs.reserve(fes->mesh()->numCells());
}




/**
 * Assembles the BilinearForm by summing over all registered integrators
 * 
 **/
void BilinearForm::assemble(){
    vector<int> adof;
    vector<int> bdof;
    MatrixXd a;

    // use registered domain integrators to integrate over cells
    if(dbfi.size()){
        // begin cell loop
        for(CellIterator c(*fes->_mesh); !c.end(); ++c){
            // get dof indices for test and trial function on the closure of the cell. these are the
            // same for Galerkin fem but not if we were to do a Petrov-Galerkin formulation
            adof = fes->closureDofs(*c);
            bdof = fes->closureDofs(*c);
            // visit each registered integrator
            for(BilinearFormIntegrator *I : dbfi){
                a = MatrixXd::Zero(adof.size(), bdof.size());
                // integrate to get element matrix
                I->integrate(*c, a);
                // accumulate into element matrix
                add(*c, a);
            }
        }
    }
    Amat.setFromTriplets(coeffs.begin(), coeffs.end());
}



/**
 * Scatters the local matrix over a MeshEntity to the global matrix by addition. This actually
 * does not directly add to the matrix; rather, it adds to a list of triplets (i,j,v_ij), which
 * the sparse matrix is later created from.
 * 
 * @param e MeshEntity
 * @param a local entity matrix
 * 
 **/
void BilinearForm::add(MeshEntity &e, Ref<MatrixXd> a){
    vector<int> adofs = fes->closureDofs(e);
    vector<int> bdofs = fes->closureDofs(e);
    for(int j = 0; j < a.cols(); j++){
        if(fes->ID[bdofs[j]] < 0) continue; // skip column if negative
        for(int i = 0; i < a.rows(); i++){
            if(fes->ID[adofs[i]] < 0) continue; // skip row if negative
            coeffs.push_back(Triplet<double>(fes->ID[adofs[i]], fes->ID[bdofs[j]], a(i,j)));
        }
    }
}




/**
 * Adds an integrator over the domain
 * 
 * @param i BilinearFormIntegrator
 * 
 **/
void BilinearForm::addDomainIntegrator(BilinearFormIntegrator *i){
    i->A = this;
    i->tabulateElement();
    dbfi.push_back(i);
}



/**
 * Returns a reference to the sparse matrix associated with the BilinearForm
 * 
 **/
SparseMatrix<double> & BilinearForm::matrix(){
    return Amat;
}



/**
 * Returns the FiniteElementSpace
 * 
 **/
FiniteElementSpace * BilinearForm::space(){
    return fes;
}