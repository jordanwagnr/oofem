#ifndef Form_h
#define Form_h
#include "FiniteElementSpace.h"
#include "Integrator.h"
#include <Eigen/Sparse>

namespace fem{

    class LinearFormIntegrator;
    class BilinearFormIntegrator;

    class LinearForm: public FiniteElementFunction{
    protected:
        friend class LinearFormIntegrator;
        
        std::vector<LinearFormIntegrator *> dlfi;

    public:
        LinearForm(FiniteElementSpace &s, bool c = true);
        
        void assemble();
        void addDomainIntegrator(LinearFormIntegrator *i);

    };


    class BilinearForm{
    protected:
        friend class BilinearFormIntegrator;

        Eigen::SparseMatrix<double> Amat;
        std::vector<Eigen::Triplet<double>> coeffs;
        FiniteElementSpace *fes;
        std::vector<BilinearFormIntegrator *> dbfi;

    public:
        BilinearForm(FiniteElementSpace &s);

        void assemble();
        void add(MeshEntity &e, Eigen::Ref<Eigen::MatrixXd> a);
        void addDomainIntegrator(BilinearFormIntegrator *i);
        Eigen::SparseMatrix<double> & matrix();
        FiniteElementSpace * space();

    };

}

#endif