#include "Integrator.h"

using namespace fem;
using namespace std;
using namespace Eigen;



/**
 * Constructs a LinearFormIntegrator
 * 
 * @param q Quadrature rule
 * 
 **/
LinearFormIntegrator::LinearFormIntegrator(Quadrature &q)
:   quad(&q)
{
    tabulateQuadrature();
}



/**
 * Tabulates the FiniteElement information needed for integration
 * 
 **/
void LinearFormIntegrator::tabulateElement(){
    // tabulate basis functions with the FE in the space related to the form
    N = L->fes->fe->tabulateBasisValues(*quad);
    dN = L->fes->fe->tabulateBasisDerivatives(*quad);
}



/**
 * Tabulates J^-T and detJ at the quadrature points for a particular MeshEntity
 * 
 * @param e MeshEntity
 * 
 **/
void LinearFormIntegrator::tabulateGeometry(MeshEntity &e){
    Jinvt.resize(quad->numPoints());
    detJ.resize(quad->numPoints());
    for(int q = 0; q < quad->numPoints(); q++){
        Jinvt[q] = L->fes->mapping().JinvT(quad->points()[q], e);
        detJ[q] = L->fes->mapping().detJ(quad->points()[q], e);
    }
}



/**
 * Tabulates the quad points and weight
 * 
 * @param q Quadrature
 * 
 **/
void LinearFormIntegrator::tabulateQuadrature(){
    w = quad->weights();
    qpts = quad->points();
}


/**
 * Constructs a LoadIntegrator with a given ScalarFunction, i.e. l(v) = (f, v)
 * 
 * @param f load function
 * 
 **/
LoadIntegrator::LoadIntegrator(ScalarFunction &f, Quadrature &q)
:   LinearFormIntegrator(q),
    func(&f)
{}




/**
 * Performs the integration over the MeshEntity and updates the element vector
 *
 * @param e MeshEntity to integrate over
 * @param l local element vector 
 * 
 **/
void LoadIntegrator::integrate(MeshEntity &e, Ref<VectorXd> l){
    // update the geometry information for the cell
    tabulateGeometry(e);
    // push forward the quadrature points to the function on global space
    RowVectorXd fq = L->space()->mapping().pushForward(*func, *quad, e);
    // evaluate the integral
    for(int q = 0; q < quad->numPoints(); q++){
        l += fq[q]*N[q]*w[q]*detJ[q];
    }
}





/**
 * Creates a BilinearForm base class
 * 
 * @param q Quadrature
 * 
 **/
BilinearFormIntegrator::BilinearFormIntegrator(Quadrature &q)
:   quad(&q)
{}



/**
 * Tabulates the FiniteElement information needed for integration
 * 
 **/
void BilinearFormIntegrator::tabulateElement(){
    // tabulate basis functions with the FE in the space related to the form
    N = A->fes->fe->tabulateBasisValues(*quad);
    dN = A->fes->fe->tabulateBasisDerivatives(*quad);
}



/**
 * Tabulates J^-T and detJ at the quadrature points for a particular MeshEntity
 * 
 * @param e MeshEntity
 * 
 **/
void BilinearFormIntegrator::tabulateGeometry(MeshEntity &e){
    Jinvt.resize(quad->numPoints());
    detJ.resize(quad->numPoints());
    for(int q = 0; q < quad->numPoints(); q++){
        Jinvt[q] = A->fes->mapping().JinvT(quad->points()[q], e);
        detJ[q] = A->fes->mapping().detJ(quad->points()[q], e);
    }
}



/**
 * Tabulates the quad points and weight
 * 
 * @param q Quadrature
 * 
 **/
void BilinearFormIntegrator::tabulateQuadrature(){
    w = quad->weights();
    qpts = quad->points();
}




/**
 * Creates a DiffusionIntegrator, which evaluates a(v,u) = (grad v, grad u)
 * 
 * @param q Quadrature
 * 
 **/
DiffusionIntegrator::DiffusionIntegrator(Quadrature &q)
:   BilinearFormIntegrator(q),
    kappa( new Constant(1.))
{}




/**
 * Creates a DiffusionIntegrator, which evaluates a(v,u) = (k grad v, grad u)
 * 
 * @param k diffusion coeffient function (conductivity) 
 * 
 **/
DiffusionIntegrator::DiffusionIntegrator(ScalarFunction &k, Quadrature &q)
:   BilinearFormIntegrator(q),
    kappa(&k)
{}



/**
 * Integrates over the MeshEntity and updates element matrix
 * 
 * @param e MeshEntity
 * @param a element matrix
 * 
 **/
void DiffusionIntegrator::integrate(MeshEntity &e, Ref<MatrixXd> a){
    // update the geometry information for the current entity
    tabulateGeometry(e);
    // push forward the quadrature points to the coefficient function defined in global domain
    RowVectorXd kq = A->space()->mapping().pushForward(*kappa, *quad, e);    
    // evaluate the integral
    for(int q = 0; q < quad->numPoints(); q++){
        // compute element metric tensor
        MatrixXd G = Jinvt[q].transpose()*Jinvt[q];
        a += kq[q]*dN[q]*G*dN[q].transpose()*detJ[q];
    }


}