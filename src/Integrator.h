#ifndef Integrator_h
#define Integrator_h
#include "Form.h"
#include "Quadrature.h"

namespace fem{

    class LinearForm;
    class BilinearForm;


    class LinearFormIntegrator{
    protected:
        
        friend class LinearForm;

        LinearForm *L;
        Quadrature *quad;
        std::vector<Eigen::VectorXd> N;
        std::vector<Eigen::MatrixXd> dN;
        std::vector<Eigen::MatrixXd> Jinvt;
        std::vector<double> detJ;
        std::vector<double> w;
        std::vector<Point> qpts;

    public:
        
        LinearFormIntegrator(Quadrature &q);

        virtual void integrate(MeshEntity &e, Eigen::Ref<Eigen::VectorXd> l) = 0; 
        void tabulateElement();
        void tabulateGeometry(MeshEntity &e);
        void tabulateQuadrature();

    };


    class LoadIntegrator : public LinearFormIntegrator{
    protected:
        ScalarFunction *func;
        
    public:
        LoadIntegrator(ScalarFunction &f, Quadrature &q);

        void integrate(MeshEntity &e, Eigen::Ref<Eigen::VectorXd> l);

    };



    class BilinearFormIntegrator{
    protected:

        friend class BilinearForm;

        BilinearForm *A;
        Quadrature *quad;
        std::vector<Eigen::VectorXd> N;
        std::vector<Eigen::MatrixXd> dN;
        std::vector<Eigen::MatrixXd> Jinvt;
        std::vector<double> detJ;
        std::vector<double> w;
        std::vector<Point> qpts;


    public:
        BilinearFormIntegrator(Quadrature &q);

        virtual void integrate(MeshEntity &e, Eigen::Ref<Eigen::MatrixXd> a) = 0; 
        void tabulateElement();
        void tabulateGeometry(MeshEntity &e);
        void tabulateQuadrature();

    };


    class DiffusionIntegrator : public BilinearFormIntegrator{
    protected:
        ScalarFunction *kappa;

    public:
        DiffusionIntegrator(Quadrature &q);
        DiffusionIntegrator(ScalarFunction &k, Quadrature &q);
    
        void integrate(MeshEntity &e, Eigen::Ref<Eigen::MatrixXd> a);
    };

}

#endif