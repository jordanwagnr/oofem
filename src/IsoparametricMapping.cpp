#include "IsoparametricMapping.h"

using namespace fem;
using namespace std;
using namespace Eigen;


/**
 * Constructs an IsoparametricMapping from a reference FiniteElement to a MeshEntity in a given
 * Mesh
 * 
 * @param f FiniteElement to map
 * @param m Mesh to map to
 * 
 **/
IsoparametricMapping::IsoparametricMapping(FiniteElement &f, Mesh &m)
:   fe(&f),
    _mesh(&m)
{}



/**
 * Evaluates the mapping
 * 
 * @param x Point in the reference element
 * @param e physical element
 * 
 **/
Point & IsoparametricMapping::operator()(Point x, MeshEntity &e){
    return map(x,e);
}



/**
 * Takes a point in the reference element to the physical element
 * 
 * @param x Point in the reference element
 * @param e physical element
 * 
 **/
Point & IsoparametricMapping::map(Point x, MeshEntity &e){
    // use element basis functions to interpolate the mesh vertex values
    VectorXd N = fe->shapeValues(x);
    vector<int> vidx = e.vertices();
    VectorXd xphys = VectorXd::Zero(_mesh->dim);
    int i = 0;
    for(VertexIterator v(*_mesh, vidx); !v.end(); ++v){
        xphys += v->point()()*N(i);
        i++;
    }
    return image(xphys);
}



/**
 * Pushes forward a node for a ScalarFunction defined in global space
 * 
 * @param f ScalarFunction
 * @param x nodal Point in reference space
 * @param e MeshEntity containing the node
 * 
 **/
double IsoparametricMapping::pushForward(ScalarFunction &f, Point x, MeshEntity &e){
    return f(map(x,e));
}


/**
 * Push forward the points in a Quardature for a ScalarFunction defined in global space
 * 
 * @param f ScalarFunction
 * @param q Quadrature
 * @param e MeshEntity
 * 
 **/
RowVectorXd IsoparametricMapping::pushForward(ScalarFunction &f, Quadrature &q, MeshEntity &e){
    RowVectorXd fq(q.numPoints());
    int i = 0;
    for(Point & qp : q.points()){
        fq(i) = f(map(qp, e));
        i++;
    }
    return fq;
}


/**
 * Computes the Jacobian matrix of the transformation at a point 
 * 
 * @param x Point in the reference element
 * @param e physical element
 * 
 **/
MatrixXd IsoparametricMapping::J(Point x, MeshEntity &e){
    MatrixXd jac = MatrixXd::Zero(x.numComponents(), _mesh->dim);
    MatrixXd dN = fe->shapeDerivatives(x);
    vector<int> vidx = e.vertices();
    int i = 0;
    for(VertexIterator v(*_mesh, vidx); !v.end(); ++v){
        jac += v->point()()*dN.row(i);
        i++;
    }
    return jac;
}



/**
 * Computes the inverse transpose of the Jacobian matrix
 * 
 * @param x Point in the reference element
 * @param e physical element
 * 
 **/
MatrixXd IsoparametricMapping::JinvT(Point x, MeshEntity &e){
    MatrixXd j = J(x,e);
    return J(x,e).transpose().inverse();
}




/**
 * Computes the Jacobian determinant of the mapping
 * 
 * @param x Point in the reference element
 * @param e physical element
 * 
 **/
double IsoparametricMapping::detJ(Point x, MeshEntity &e){
    return J(x,e).determinant();
}