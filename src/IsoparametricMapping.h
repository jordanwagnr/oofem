#ifndef IsoparametricMapping_h
#define IsoparametricMapping_h
#include "FiniteElement.h"
#include "Mesh.h"
#include "Expression.h"

namespace fem{
    
    class IsoparametricMapping{
    protected:
        FiniteElement *fe;
        Mesh *_mesh;
        Point image;

    public:
        IsoparametricMapping(){};
        IsoparametricMapping(FiniteElement &f, Mesh &m);

        Point & operator()(Point x, MeshEntity &e);
        Point & map(Point x, MeshEntity &e);
        double pushForward(ScalarFunction &f, Point x, MeshEntity &e);
        Eigen::RowVectorXd pushForward(ScalarFunction &f, Quadrature &q, MeshEntity &e);
        Eigen::MatrixXd J(Point x, MeshEntity &e);
        Eigen::MatrixXd JinvT(Point x, MeshEntity &e);
        double detJ(Point x, MeshEntity &e);
        

    };
}


#endif