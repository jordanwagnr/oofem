#include "Mesh.h"

using namespace fem;
using namespace std;


/**
 * Construct a mesh by reading a .msh file
 * 
 * @param f mesh filename
 * 
 **/
Mesh::Mesh(std::string f){
    loadGmsh(f);
    // number vertices trivially
    _offsets[dim].resize(numVertices());
    _topology[dim].resize(numVertices());
    for(int i = 0; i < numVertices(); i++){
        _offsets[dim][i] = i;
        _topology[dim][i] = i;
    }
    nve[dim].resize(numVertices());
    for(int &i : nve[dim]){
        i = 1;
    }

    // get number of entities of each dimension
    nents.resize(dim+1);
    for(int k = 0; k <= dim; k++){
        nents[k] = _offsets[dim-k].size();
    }
}


/**
 * Returns the dimension
 * 
 **/
int Mesh::dimension(){
    return dim;
}



/**
 * Returns the number of elements
 * 
 **/
int Mesh::numElmts(){
    return numelmts;
}



/**
 * Returns the number of cells
 * 
 **/
int Mesh::numCells(){
    return numcells;
}



/**
 * Returns the number of facets
 * 
 **/
int Mesh::numFacets(){
    return numfacets;
}



/**
 * Returns the number of vertices
 * 
 **/
int Mesh::numVertices(){
    return numverts;
}


/**
 * Returns a vector of the number of entities of each dimension
 * 
 **/
std::vector<int> Mesh::numEntities(){
    return nents;
}



/**
 * Prints basic mesh information
 * 
 **/
void Mesh::print(){
    cout << endl << "--------------------------------------------------" << endl;
    cout << "MESH INFORMATION" << endl;
    cout << "Dimension: " << dim << endl;
    cout << "Number of cells: " << numcells << endl;
    cout << "Number of facets: " << numfacets << endl;
    cout << "Number of vertices: " << numverts <<  endl;
    cout << "--------------------------------------------------" << endl;
    cout << endl << endl;
}



/**
 * Prints the mesh topology of a given topological dimension 
 * 
 * @param k topological dimension 
 * 
 **/
void Mesh::printTopology(int k){
    cout << k << "-_topology:" << endl;
    for(int &i : _topology[dim-k]){
        cout << i << endl;
    }
}


/**
 * Returns a reference to the topology array of a give topological dimension. This doesn't copy the
 * data.
 *
 * @param k topological dimension
 *
 **/
vector<int> & Mesh::topology(int k){
    return _topology[dim - k];    
}



/**
 * Returns a reference to the offsets array for a given dimension, which maps a mesh entity index to
 * an offset into the topology array. This doesn't copy the data.
 *
 * @param k topological dimension
 *
 **/
vector<int> & Mesh::offsets(int k){
    return _offsets[dim-k];
}



/**
 * Returns a reference to the coordinate array.
 * 
 **/
Eigen::Map<Eigen::VectorXd> Mesh::coordinates(){
    return Eigen::Map<Eigen::VectorXd> (_coordinates.data(), _coordinates.size());
}


/**
 * Returns a Point corresponding to a mesh vertex
 * 
 * 
 * @param v vertex number 
 * 
 **/
Point Mesh::point(int v){
    return Point(_coordinates.segment(v*dim, dim));
}



/**
 * Loads a gmsh file to the current Mesh.
 * 
 * @param f filename of the .msh file.
 * 
 **/
void Mesh::loadGmsh(string f){
    GmshReader r(this, f);
    r.readNumElmts();
    r.readElmts();
    r.readVertices();
}


/**
 * Creates and registers a MeshTag. If a tag with the same value and dimension  is already in the
 * register, it is overwritten.
 *
 * @param v tag value
 * @param k dimension of entities to tag
 * @param i indices of entities to tag
 *
 **/
void Mesh::createTag(int v, int k, vector<int> i){
    tagregister[dim-k][v] = new MeshTag(*this, v, k, i); 
}



/**
 * Searches the registered tags for a value v marking k-dimensional entities and returns a reference
 * to the correspond tag. If the tag does not exist, an exception is thrown.
 *
 * @param 
 *
 **/
MeshTag & Mesh::getTag(int v, int k){
    return *tagregister[dim-k].at(v);
}



/**
 * Returns whether a given tag already exists in the register
 * 
 * @param t MeshTag value
 * @param k dimension of entities in MeshTag
 * 
 **/
bool Mesh::hasTag(int t, int k){
    MeshTag * tag = tagregister[dim-k][t];
    if(tag){
        return true;
    }
    return false;
}


/**
 * Returns the type of the e-th entity (in gmsh format) in the k-topology array. 
 * 
 * @param e entity index
 * @param k entity dimension
 * 
 **/
int Mesh::entityType(int e, int k){
    return types[dim-k][e];
}



/**
 * Returns the geometry type of the cells. Hybrid meshes are currently not implemted.
 * 
 **/
GEOM_t Mesh::cellType(){
    switch(types[0][0]){
        case 1: return LINE;
        case 2: return TRIA;
        case 3: return QUAD;
        case 15: return POINT;
        default: cout << "Cell type not recognized." << endl;
    }
    return NONE;
}



/**
 * Returns the geometry type of the facets. Hybrid meshes are currently not implemted.
 * 
 **/
GEOM_t Mesh::facetType(){
    switch(types[1][0]){
        case 1: return LINE;
        case 2: return TRIA; 
        case 3: return QUAD; 
        case 15: return POINT;
        default: cout << "Facet type not recognized." << endl;
    }
    return NONE;
}