#ifndef Mesh_h
#define Mesh_h
#include <vector> 
#include <Eigen/Dense>   
#include <string>
#include "FileReader.h"
#include "MeshEntity.h"
#include "Point.h"
#include "MeshTag.h"
#include <map>
#include "common.h"

namespace fem{
    class MeshTag;

    class Mesh{
    protected:
        friend class GmshReader;
        friend class MeshEntity;
        friend class Cell;
        friend class Facet;
        friend class Vertex;
        friend class MeshTag;
        template <class T> friend class MeshEntityIterator; 
        friend class IsoparametricMapping;


        int numelmts, numcells, numfacets, numverts, dim=0;
        Eigen::VectorXd _coordinates;
        std::vector<std::vector<int>> _topology;
        std::vector<std::vector<int>> _offsets;
        std::vector<std::vector<int>> nve;
        std::vector<std::vector<int>> types;
        std::vector<int> nents;
        std::vector<std::map<int, MeshTag *>> tagregister;

    public:
        Mesh(){};
        Mesh(std::string f);

        int dimension();
        int numElmts();
        int numCells();
        int numFacets();
        int numVertices();
        std::vector<int> numEntities();
        void print();
        void printTopology(int k);
        std::vector<int> & topology(int k);
        std::vector<int> & offsets(int k);
        Eigen::Map<Eigen::VectorXd> coordinates();
        Point point(int v);
        void loadGmsh(std::string f);
        void createTag(int t, int k, std::vector<int> i);
        MeshTag & getTag(int t, int k);
        bool hasTag(int t, int k);
        int entityType(int e, int k);
        GEOM_t cellType();
        GEOM_t facetType();

    };
}
#endif