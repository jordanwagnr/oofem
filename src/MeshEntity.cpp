#include "MeshEntity.h"

using namespace std;
using namespace Eigen;
using namespace fem;


/**
 * Constructs a MeshEntity in a Mesh with a given index, dim.
 * 
 * @param m Mesh
 * @param i index
 * @param d topological dimension
 * 
 **/
MeshEntity::MeshEntity(Mesh &m, int i, int d)
:   nverts(m.nve[m.dim-d][i]),
    dim(d),
    idx(i),
    msh(&m)
{
    assert(idx < m.numEntities()[d]);
}



/**
 * Constructs a MeshEntity in a Mesh with a given index, dim, and nvert.
 * 
 * @param m Mesh
 * @param i index
 * @param d topological dimension
 * @param n number of attached vertices
 * 
 **/
MeshEntity::MeshEntity(Mesh &m, int i, int d, int n)
:   nverts(n),
    dim(d),
    idx(i),
    msh(&m)
{
    assert(idx < m.numEntities()[d]);
}



/**
 * Returns number of vertices attached
 * 
 **/
int MeshEntity::numVertices(){
    return nverts;
}


/**
 * Returns dimension of entity
 * 
 **/
int MeshEntity::dimension(){
    return dim;
}


/**
 * Returns index
 * 
 **/
int MeshEntity::index(){
    return idx;
}


/**
 * Returns a pointer to the owner Mesh
 * 
 **/
Mesh * MeshEntity::mesh(){
    return msh;
}



/**
 * Returns the physical coordinates of a particular vertex making up the cell.
 * 
 * @param v local vertex number (e.g. v = 1,2, or 3 for triangles)
 * 
 **/
Point MeshEntity::point(int v){
    assert(v < nverts && v >= 0);
    return msh->point(vertices()[v]);
}


/**
 * Constructs a cell entity with a given index
 * 
 * @param i index
 * 
 **/
Cell::Cell(Mesh &m, int i)
:   MeshEntity(m, i, m.dimension())
{
    switch(m.entityType(i,m.dimension())){
        case 1: nverts = 2; break;
        case 2: nverts = 3; break;
        case 3: nverts = 4; break;
        case 15: nverts = 1; break;
        default: throw "Cell type not recognixed in MeshEntity construction";
    }
}


/**
 * Returns an array of the incident nodes
 * 
 **/
vector<int> Cell::vertices(){
    vector<int> verts(nverts);
    for(int i = 0; i < nverts; i++){
        verts[i] = msh->_topology[0][msh->_offsets[0][idx]+i]; 
    }
    return verts;
}



/**
 * Constructs a facet entity with a given index
 * 
 * @param i index
 * 
 **/
Facet::Facet(Mesh &m, int i)
:   MeshEntity(m, i, m.dimension()-1)
{
    switch(m.entityType(i,m.dimension()-1)){
        case 1: nverts = 2; break;
        case 2: nverts = 3; break;
        case 3: nverts = 4; break;
        case 15: nverts = 1; break;
        default: throw "Facet type not recognixed in MeshEntity construction";
    }
}




/**
 * Returns an array of the incident nodes
 * 
 **/
vector<int> Facet::vertices(){
    vector<int> verts(nverts);
    for(int i = 0; i < nverts; i++){
        verts[i] = msh->_topology[1][msh->_offsets[1][idx]+i]; 
    }
    return verts;
}



/**
 * Constructs a vertex entity with a given index
 * 
 * @param i index
 * 
 **/
Vertex::Vertex(Mesh &m, int i)
:   MeshEntity(m, i, 0)
{
    nverts = 1;
}




/**
 * Returns an array of the incident vertices. This is trivial for vertices.
 * 
 **/
vector<int> Vertex::vertices(){
    vector<int> verts(nverts);
    for(int i = 0; i < nverts; i++){
        verts[i] = msh->_topology[msh->dim][msh->_offsets[msh->dim][idx]+i]; 
    }
    return verts;
}
