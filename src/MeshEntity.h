#ifndef MeshEntity_h
#define MeshEntity_h
#include <Eigen/Dense>
#include "Mesh.h"
#include "Point.h"
#include "MeshEntityIterator.h"

namespace fem{

    class Mesh;
    //template <class T> class MeshEntityIterator;

    class MeshEntity{
    protected:
        template<class T> friend class MeshEntityIterator;
        
        int nverts, dim, idx;
        Mesh *msh;

    public:
        MeshEntity(){};
        MeshEntity(Mesh &, int i, int d);
        MeshEntity(Mesh &, int i, int d, int n);

        int numVertices();
        int dimension();
        int index();
        Mesh * mesh();
        Point point(int v = 0);
        virtual std::vector<int> vertices() = 0;
    };


    class Cell: public MeshEntity{
    public:
        //typedef MeshEntityIterator<Cell> CellIterator;
        
        Cell(Mesh &, int);
        
        std::vector<int> vertices();
    };


    class Facet: public MeshEntity{
    public:            
        Facet(Mesh &, int);

        std::vector<int> vertices();
        //VertexIterator verts(){};
    };


    class Vertex: public MeshEntity{
    public:
        Vertex(Mesh &, int);

        std::vector<int> vertices();
    };

}


#endif
