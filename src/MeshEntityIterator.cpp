#include "MeshEntityIterator.h"

using namespace std;
using namespace Eigen;
using namespace fem;

/**
 * Constructs an iterator of the all entites of given dimension over the entire mesh
 * 
 * @param m Mesh
 * 
 **/
template <class T>
MeshEntityIterator<T>::MeshEntityIterator(Mesh &m)
:   _entity(m, 0),
    _pos(0)
{
    // indices are over the entire mesh
    _idx.resize(m._offsets[m.dim-_entity.dim].size());
    for(int i = 0; i < (int) _idx.size(); i++){
        _idx[i] = i;
    }
    _end = _idx.size(); 
}



/**
 * Constructs an iterator of entites of given dimension that are tagged by a particular value
 * 
 * @param m Mesh
 * @param t tag value
 * 
 **/
template <class T>
MeshEntityIterator<T>::MeshEntityIterator(Mesh &m, int t)
:   _entity(m, 0),
    _pos(0)
{
    // indices are over only those that are tagged
    _idx = m.getTag(t, _entity.dim).indices();
    _end = _idx.size(); 
}



/**
 * Constructs an iterator of entities of given dimension over a given set of indices
 * 
 * @param m Mesh
 * @param i mesh indices to iterate over
 * 
 **/
template <class T>
MeshEntityIterator<T>::MeshEntityIterator(Mesh &m, vector<int> &i)
:   _entity(m,i[0]),
    _idx(i),
    _pos(0),
    _end(_idx.size())
{}




/**
 * Increment the position of the iterator
 * 
 **/
template <class T>
MeshEntityIterator<T> & MeshEntityIterator<T>::operator++(){
    ++_pos;
    return *this;
}



/**
 * Decrement the position of the iterator
 * 
 **/
template <class T>
MeshEntityIterator<T> & MeshEntityIterator<T>::operator--(){
    --_pos;
    return *this;
}


/**
 * Return the position of the iterator
 * 
 **/
template <class T>
int MeshEntityIterator<T>::position(){
    return _pos;
}


/**
 * Dereference the iterator
 * 
 **/
template <class T>
T & MeshEntityIterator<T>::operator*(){
    return *operator->();
}



/**
 * Member access operator
 * 
 * @param 
 * 
 **/
template <class T>
T * MeshEntityIterator<T>::operator->(){
    _entity.idx = _idx[_pos];
    return &_entity;
}


/**
 * Comparison operator
 * 
 * @todo finish this
 * 
 * @param it MeshEntityIterator to compare to
 * 
 **/
template <class T>
bool MeshEntityIterator<T>::operator==(MeshEntityIterator &it){
    return true;
}


/**
 * Comparison operator
 * 
 * 
 * @param it MeshEntityIterator to compare to
 * 
 **/
template <class T>
bool MeshEntityIterator<T>::operator!=(MeshEntityIterator &it){
    return !operator==(it);
}


/**
 * Returns whether or not the position is at the end
 * 
 **/
template <class T>
bool MeshEntityIterator<T>::end(){
    return _pos >= _end;
}



// EXPLICIT INSTANTIATIONS
template class MeshEntityIterator<Cell>;
template class MeshEntityIterator<Facet>;
template class MeshEntityIterator<Vertex>;