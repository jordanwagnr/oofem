#ifndef MeshEntityIterator_h
#define MeshEntityIterator_h
#include "MeshEntity.h"

namespace fem{
    class Mesh;
    class MeshEntity;
    class Cell;
    class Facet;
    class Vertex;

    template <class T>
    class MeshEntityIterator{
    protected:
        T _entity;
        std::vector<int> _idx;
        int _pos;
        int _end;

    public:
        MeshEntityIterator(Mesh &m);
        MeshEntityIterator(Mesh &m, int t);
        MeshEntityIterator(Mesh &m, std::vector<int> &i);

        MeshEntityIterator & operator++();
        MeshEntityIterator & operator--();
        int position();
        T & operator*();
        T * operator->();
        bool operator==(MeshEntityIterator &it);
        bool operator!=(MeshEntityIterator &it);
        bool end();
        MeshEntityIterator end_iterator();
    };

    typedef MeshEntityIterator<Cell> CellIterator;
    typedef MeshEntityIterator<Facet> FacetIterator;
    typedef MeshEntityIterator<Vertex> VertexIterator;
}

#endif 