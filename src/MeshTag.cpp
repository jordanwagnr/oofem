#include "MeshTag.h"
#include <iostream>
#include <assert.h>

using namespace std;
using namespace fem;

/**
 * Constructs a MeshTag for k-dimensional mesh entities.
 * 
 * @param m Mesh
 * @parm v tag value
 * @param k dimension of the mesh entities to tag
 * @param i indices of the entities to tag
 * 
 **/
MeshTag::MeshTag(Mesh &m, int v, int k, vector<int> i)
:   dim(k),
    _indices(i),
    _value(v),
    msh(&m)
{
    // check that the indices given are compatible
    assert(_indices.size() <= m._offsets[m.dim - k].size());
    int max = *max_element(_indices.begin(), _indices.end());
    assert(max <= (int) m._offsets[m.dim - k].size()); 
}



/**
 * Returns the name of the tag
 * 
 **/
string MeshTag::name(){
    return _name;
}



/**
 * Sets a name to the tag
 * 
 * @param n name
 * 
 **/
void MeshTag::name(string n){
    _name = n;
}

/**
 * Returns the dimension of the entities that this tag marks
 * 
 **/
int MeshTag::dimension(){
    return dim;
}


/**
 * Returns the value of the tag
 * 
 **/
int MeshTag::value(){
    return _value;
}


/**
 * Returns a reference to the tagged indices
 * 
 **/
vector<int> & MeshTag::indices(){
    return _indices;
}



/**
 * Returns a reference to the Mesh that contains this tag
 * 
 **/
Mesh & MeshTag::mesh(){
    return *msh;
}


/**
 * Prints the MeshTag to the screen. Passing true will print the compact form, which print
 * everything but the indices.
 *
 * @param c print compact? (default is false)
 * 
 **/
void MeshTag::print(bool c){
    cout << "--------------------------------------------------" << endl;
    cout << "TAG INFORMATION" << endl;
    if(this){
        if(!_name.empty()){
            cout << "Name: " << _name << endl; 
        }
        else{
            cout << "Name: not named" << endl;
        }
        cout << "Value: " << _value << endl;
        cout << "Dimension: " << dim << endl;
        cout << "Number of entities: " << _indices.size() << endl;

        if(!c){
            cout << "Indices: " << endl;
            for(int i = 0; i < (int) _indices.size(); i++){
                cout << _indices[i] << endl;
            }
        }
    }
    else{
        cout << "NULL" << endl;
    }
    cout << "--------------------------------------------------" << endl;
    cout << endl;
}
