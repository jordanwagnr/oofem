#ifndef MeshTag_h
#define MeshTag_h
#include <vector>
#include <string>
#include "Mesh.h"

namespace fem{
    class Mesh;

    class MeshTag{
    protected:
        int dim = 0;
        std::vector<int> _indices;
        int _value;
        Mesh *msh;
        std::string _name;

    public:
        MeshTag(){};
        MeshTag(Mesh &msh, int v, int k, std::vector<int> i);

        std::string name();
        void name(std::string n);
        int dimension();
        int value();
        std::vector<int> & indices();
        Mesh & mesh();
        void print(bool c = false);

    };
}
#endif