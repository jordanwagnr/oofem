#include "Point.h"

using namespace Eigen;
using namespace std;
using namespace fem;

/**
 * Constructs a point with a vector of coordinates
 * 
 * @param x coordinates
 * 
 **/
Point::Point(Ref<VectorXd> x){
    ncomps = x.size();
    assert(ncomps <= 3);
    coords.head(ncomps) = x;
}


/**
 * Constructs a point in 1-D space with a coordinate
 * 
 * @param x coordinate in 1-D space
 * 
 **/
Point::Point(double x){
    ncomps = 1;
    coords(0) = x;
}



/**
 * Constructs a point in 2-D space with a coordinate
 * 
 * @param x coordinate in 2-D space
 * 
 **/
Point::Point(double x1, double x2){
    ncomps = 2;
    coords(0) = x1;
    coords(1) = x2;
}



/**
 * Constructs a point in 2-D space with a coordinate
 * 
 * @param x coordinate in 2-D space
 * 
 **/
Point::Point(double x1, double x2, double x3){
    ncomps = 3;
    coords(0) = x1;
    coords(1) = x2;
    coords(2) = x3;
}



/**
 * Assignment operator
 * 
 * @param x Point to assign
 * 
 **/
Point & Point::operator=(const Point &x){
    coords = x.coords;
    ncomps = x.ncomps;
    return *this;
}


/**
 * Operator () to return the coordinate vector of the point
 * 
 **/
Ref<VectorXd> Point::operator()(){
    return coords.head(ncomps);
}


/**
 * Operator () to return the a component of the coordinate vector of the point
 * 
 * @param c component index
 * 
 **/
double Point::operator()(int c){
    return coords(c);
}


/**
 * Updates the Point with the given coordinates, then returns the point
 * 
 * @param x coordinates
 * 
 **/
Point & Point::operator()(Ref<VectorXd> x){
    ncomps = x.size();
    assert(ncomps <= 3);
    coords.head(ncomps) = x;
    return *this;
}



/**
 * Scalar mutipliciation, ax
 * 
 * @param a scalar value
 * 
 **/
Point & Point::operator*(double a){
    coords *= a;
    return *this;
}




/**
 * Returns the number of components
 * 
 **/
int Point::numComponents(){
    return ncomps;
}



/**
 * Returns the L2 norm of the point
 * 
 **/
double Point::norm(){
    return (*this)().norm();
}