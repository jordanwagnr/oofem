#ifndef Point_h
#define Point_h
#include <iostream>
#include <Eigen/Dense>

namespace fem{
    class Point{
    protected:
        Eigen::Vector3d coords;
        int ncomps=3;

    public:
        Point(){};
        Point(Eigen::Ref<Eigen::VectorXd>);
        Point(double x);
        Point(double x1, double x2);
        Point(double x1, double x2, double x3);
        Point(const Point &p): coords(p.coords), ncomps(p.ncomps) {}; 

        friend std::ostream & operator<<(std::ostream &s, Point x){
            s << x() << std::endl;
            return s; 
        }
        Eigen::Ref<Eigen::VectorXd>  operator()();
        double operator()(int c);
        Point & operator()(Eigen::Ref<Eigen::VectorXd> x);
        Point & operator*(double a);
        Point & operator+(const Point &x);
        Point & operator=(const Point &x);
        double norm();

        int numComponents();

    };
}
#endif