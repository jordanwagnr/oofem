#include "Quadrature.h"

using namespace fem;
using namespace std;


/**
 * Creates a quadrature rule with given points and weights
 * 
 * @param q vector of quadrature points
 * @param w vector of quadrature weights
 * @param g reference cell geometry
 * 
 **/
Quadrature::Quadrature(vector<Point> &q, vector<double> &w, GEOM_t g)
:   qpoints(q),
    _weights(w),
    refgeom(g)
{}



/**
 * Returns a vector of quadrature points
 * 
 **/
vector<Point> & Quadrature::points(){
    return qpoints;
}



/**
 * Returns a vector of quadrature weights
 * 
 **/
vector<double> & Quadrature::weights(){
    return _weights;
}


/**
 * Returns the number of quadrature points
 * 
 **/
int Quadrature::numPoints(){
    return npoints;
}



/**
 * Creates a Gauss quadrature of specific order over a reference cell. You need n points to
 * integrate a 2n-1 polynomial in each direction. 
 *
 * @param g reference cell geometry
 * @param n number of total points
 *
 **/
GaussQuadrature::GaussQuadrature(GEOM_t g, int n){
    refgeom = g;
    npoints = n;
    qpoints.resize(n);
    _weights.resize(n);

    switch(refgeom){
        case LINE: setLine_internal(); break;
        case TRIA: setTria_internal(); break;
        case QUAD: setQuad_internal(); break;
        default: throw "Quadrature reference element not implemented.";
    }


}



void GaussQuadrature::setLine_internal(){
    switch(npoints){
        case 1:{
            qpoints[0] = Point(0);
            _weights[0] = 2;
            break;
        }
        case 2:{
            qpoints[0] = Point(-1./sqrt(3));
            qpoints[1] = Point(1./sqrt(3));
            _weights[0] = 1;
            _weights[1] = 1;
            break;
        }
        default: throw "Quadrature rule not defined.";
    }
}



void GaussQuadrature::setTria_internal(){
    switch(npoints){
        case 1:{
            qpoints[0] = Point(1./3, 1./3);
            _weights[0] = 1./2;
            break;
        }
        case 3:{
            qpoints[0] = Point(1./6, 1./6);
            qpoints[1] = Point(2./3, 1./6);
            qpoints[2] = Point(1./6, 2./3);
            _weights[0] = 1./6;
            _weights[1] = 1./6;
            _weights[2] = 1./6;
            break;
        }
        case 4:{
            qpoints[0] = Point(1./3, 1./3);
            qpoints[1] = Point(1./5, 3./5);
            qpoints[2] = Point(1./5, 1./5);
            qpoints[3] = Point(3./5, 1./5);
            _weights[0] = -27./96;
            _weights[1] =  25./96;
            _weights[2] =  25./96;
            _weights[3] =  25./96;
            break;
        }
        default: throw "Quadrature rule not defined.";
    }
}



void GaussQuadrature::setQuad_internal(){
    switch(npoints){
        case 1:{
            qpoints[0] = Point(0, 0);
            _weights[0] = 4;
            break;
        }
        case 4:{
            qpoints[0] = Point(-1./sqrt(3), -1./sqrt(3));
            qpoints[1] = Point( 1./sqrt(3), -1./sqrt(3));
            qpoints[2] = Point( 1./sqrt(3),  1./sqrt(3));
            qpoints[3] = Point(-1./sqrt(3),  1./sqrt(3));
            _weights[0] = 1;
            _weights[1] = 1;
            _weights[2] = 1;
            _weights[3] = 1;
            break;
        }
        default: throw "Quadrature rule not defined.";
    }
}
