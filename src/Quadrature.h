#ifndef Quadrature_h
#define Quadrature_h
#include <vector>
#include "Point.h"
#include "common.h"

namespace fem{

    class Quadrature{
    protected:
        std::vector<Point> qpoints;
        std::vector<double> _weights;
        GEOM_t refgeom;
        int npoints;

    public:
        Quadrature(){};
        Quadrature(std::vector<Point> &q, std::vector<double> &w, GEOM_t g);

        std::vector<Point> & points();
        std::vector<double> & weights();
        int numPoints();

    };


    class GaussQuadrature: public Quadrature{
    protected:
        void setLine_internal();
        void setTria_internal();
        void setQuad_internal();

    public:
        GaussQuadrature(GEOM_t g, int o);

    };


}

#endif