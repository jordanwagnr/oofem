#ifndef common_h
#define common_h

namespace fem{
    enum GEOM_t {NONE, POINT, LINE, TRIA, QUAD};
    enum FE_t {LAGRANGE};
    enum ENTITY_t {VERTEX, FACET, CELL};

    #define pi M_PI

}

#endif