#include "Mesh.h"
#include "Point.h"
#include "Form.h"
#include "FiniteElementFunction.h"
#include "DirichletBC.h"
#include "Quadrature.h"
#include "IsoparametricMapping.h"
#include "DataWriter.h"
#include <Eigen/SparseCholesky>

using namespace std;
using namespace fem;

int main(int argc, char *argv[]){
    
    // Construct the mesh and print the information.
    Mesh mesh("meshes/square_fine.msh");
    mesh.print();

    // Construct the finite element space over the mesh
    FiniteElementSpace fes(mesh);

    // Constrain dofs that are related to specific parts of the mesh that have been tagged by GMSH
    set<int> cdof1 = fes.getTaggedDofs(2, 1);
    set<int> cdof2 = fes.getTaggedDofs(3,1);
    fes.constrainDofs(cdof1);
    fes.constrainDofs(cdof2);
    fes.buildIdArray();

    // Construct a quadrature rule
    GaussQuadrature q(TRIA, 3);
    
    // construct a constant forcing and coefficient function
    Constant f(10);
    Constant k(1);
    Constant z(0); // zero function

    // Project the zero function onto the constructed finite element space, so that we have
    // zeros defined on the constrained nodes
    FiniteElementFunction u = fes.project(z);

    // Create the bilinear form and add a diffusion integrator
    BilinearForm A(fes);
    A.addDomainIntegrator(new DiffusionIntegrator(k,q));
    A.assemble();

    // Create the linear form and add a load integrator
    LinearForm L(fes);
    L.addDomainIntegrator(new LoadIntegrator(f,q));
    L.assemble();

    // Create a linear solver context via Eigen. 
    Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>> solver;
    // Factor the matrix
    solver.compute(A.matrix());
    // Solve the system for the function coefficients
    Eigen::VectorXd &x = u.vcoeffs();
    x = solver.solve(L.coeffs());

    // Export the solution for visualization
    DataWriter writer("run", u);
    writer.writeAllData();
}
