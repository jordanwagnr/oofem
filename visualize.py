#!/usr/bin/env python3
import numpy as np
import sys
import matplotlib as mpl
mpl.use('Qt4Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection


def trisurf(x, e, uh):
    # plot trisurface
    plt.style.use("default")
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.plot_trisurf(x[:,0], x[:,1], e, uh, cmap=mpl.cm.viridis, vmin=np.amin(uh), vmax=np.amax(uh), shade=True, linewidth=.5)
    solmax = np.amax(uh)
    solmin = np.amin(uh)
    ax.set_zlim(solmin*(1 - np.sign(solmin)*.4), solmax*(1 + np.sign(solmin)*.4))


def quadsurf(nodes, elmts, sol, ax=None, **kwargs):
    # plot quadsurface

    def quatplot(nodes, sol, quatrangles, ax=None, **kwargs):
        if not ax: ax=plt.gca()
        xyz = np.c_[nodes,sol]
        verts= xyz[quatrangles]
        pc = Poly3DCollection(verts, **kwargs)
        pc.set_facecolor('w')
        pc.set_array(uh)
        ax.add_collection3d(pc)
        solmax = np.amax(sol)
        solmin = np.amin(sol)
        # add some padding
        ax.set_zlim(solmin*(1 - np.sign(solmin)*.4), solmax*(1 + np.sign(solmin)*.4))
        pc.set_clim(solmin,solmax)
        return pc

    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    pc = quatplot(nodes, uh, elmts, ax=ax, linewidth=2, alpha=.5)
    


def plotTriMesh(nodes, trias):
    fig = plt.figure()
    ax = plt.gca()
    ax.triplot(nodes[:,0],nodes[:,1],trias, linewidth=.75, color='k')

    
if __name__ == '__main__':
    if len(sys.argv) == 2:
        f = sys.argv[1]
    else:
        f = "run"

    # read topology information in solution directory
    topofn = "./" + f + "/topo"
    e = np.loadtxt(topofn, delimiter=",")
    e = e.astype(int)
    
    # read coordinate information in solution directory
    coordfn = "./" + f + "/coord"
    x = np.loadtxt(coordfn, delimiter=",")

    # read field data in solution directory
    fieldfn = "./" + f + "/field"
    uh = np.loadtxt(fieldfn)

    if(np.size(e[0,:]) == 3):
        trisurf(x, e, uh)
        plotTriMesh(x,e)
    elif(np.size(e[0,:]) == 4):
        quadsurf(x, e, uh)

    plt.show()
    

    
